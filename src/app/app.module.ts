import { MyAccSubMenuPage } from './myacc-sub-menu/myacc-sub-menu';
import { BizSubMenuPage } from './business-sub-menu/biz-sub-menu';
import { PaymentPage } from './../pages/payment/payment';
import { BookingViewPage } from './../pages/booking-view/booking-view';
import { AddoffersPage } from './../pages/offers/add-offers/add-offers';
import { OffersDetailPage } from './../pages/offers/offers-detail/offers-detail';
import { OffersPage } from './../pages/offers/offers';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { Calendar } from '@ionic-native/calendar';
import { CalendarModule } from "ion2-calendar";
import { CustomerPage } from '../pages/customer/customer';
import { HolidayPage } from '../pages/holiday/holiday';
import { AddHolidayPage } from '../pages/add-holiday/add-holiday';
import { StaffPage } from '../pages/staff/staff';
import { AddStaffPage } from '../pages/staff/add-staff/add-staff';
import { AddBankPage } from '../pages/add-bank/add-bank';
import { GallaryPage } from '../pages/gallary/gallary';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { SettingsPage } from '../pages/settings/settings';
import { utilService } from '../services/core/util-service';
import { BusinessProfilePage } from '../pages/business-profile/business-profile';
import { TimingsPage } from '../pages/timings/timings';
import { ServicesPage } from '../pages/services/services';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
import { AddServicesPage } from '../pages/services/add-services/add-services';
import { PopoverPage } from '../pages/popover/popover';
import { ImagePicker } from '@ionic-native/image-picker';
import { PackagePage } from '../pages/package/package';
import { CustomerDetailPage } from '../pages/customer/customer-detail/customer-detail';
import { CallNumber } from '@ionic-native/call-number';
import { SocialSharing } from '@ionic-native/social-sharing';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { API } from '../services/api-service';
import { SystemService } from '../services/system-service';
import { PackageDetailPage } from '../pages/package/package-detail/package-detail';
import { AddpackagePage } from '../pages/package/add-package/add-package';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations"
import { MatFormFieldModule, MatInputModule, MatButtonModule, MatSelectModule } from "@angular/material";
import { MatAutocompleteModule } from '@angular/material/autocomplete';
// import { AddNewpackagePage } from '../pages/package/add-new-package/add-new-package';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { AppErrorHandler } from './app.errorHandler';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { PortfolioPage } from '../pages/portfolio/portfolio';
import { AddPortfolio } from '../pages/portfolio/add-portfolio/add-portfolio';
import { EmailComposer } from '@ionic-native/email-composer';
import { Network } from '@ionic-native/network';
import { NoConnectionPage } from '../pages/no-connection-model/no-connection-model';
import { HolidayListPage } from '../pages/holiday-list/holiday-list';
import { AppVersion } from '@ionic-native/app-version';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    CustomerPage,
    HolidayPage,
    AddHolidayPage,
    StaffPage,
    AddStaffPage,
    AddBankPage,
    GallaryPage,
    DashboardPage,
    SettingsPage,
    BusinessProfilePage,
    TimingsPage,
    AddServicesPage,
    ServicesPage,
    PopoverPage,
    PackagePage,
    CustomerDetailPage,
    PackageDetailPage,
    AddpackagePage,
    // AddNewpackagePage,
    OffersPage,
    OffersDetailPage,
    AddoffersPage,
    TutorialPage,
    PortfolioPage,
    AddPortfolio,
    BookingViewPage,
    PaymentPage,
    BizSubMenuPage,
    MyAccSubMenuPage,
    NoConnectionPage,
    HolidayListPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    CalendarModule,
    Ng2AutoCompleteModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatAutocompleteModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    CustomerPage,
    HolidayPage,
    AddHolidayPage,
    StaffPage,
    AddStaffPage,
    AddBankPage,
    GallaryPage,
    DashboardPage,
    SettingsPage,
    BusinessProfilePage,
    TimingsPage,
    AddServicesPage,
    ServicesPage,
    PopoverPage,
    PackagePage,
    CustomerDetailPage,
    PackageDetailPage,
    AddpackagePage,
    // AddNewpackagePage,
    OffersPage,
    OffersDetailPage,
    AddoffersPage,
    TutorialPage,
    PortfolioPage,
    AddPortfolio,
    BookingViewPage,
    PaymentPage,
    BizSubMenuPage,
    MyAccSubMenuPage,
    NoConnectionPage,
    HolidayListPage

  ],
  providers: [
    StatusBar,
    SystemService,
    API,
    utilService,
    SplashScreen,
    StatusBar,
    SplashScreen,
    Calendar,
    ImagePicker,
    CallNumber,
    SocialSharing,
    FileTransfer,
    AndroidPermissions,
    Network,
    ScreenOrientation,
    EmailComposer,
    AppVersion,
    { provide: ErrorHandler, useClass: AppErrorHandler }
  ]
})
export class AppModule { }
