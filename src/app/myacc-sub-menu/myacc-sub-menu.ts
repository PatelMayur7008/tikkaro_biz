import { TutorialPage } from './../../pages/tutorial/tutorial';
import { SystemService } from './../../services/system-service';
import { AddBankPage } from './../../pages/add-bank/add-bank';
import { Component } from '@angular/core';
import { NavController, ViewController, NavParams, AlertController} from 'ionic-angular';
import * as moment from 'moment';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../../pages/login/login';

@Component({
  selector: 'page-myacc-sub-menu',
  templateUrl: 'myacc-sub-menu.html',
})
export class MyAccSubMenuPage {

  constructor(public navCtrl: NavController,public viewCtrl: ViewController,private navParams: NavParams,public systemService: SystemService,private alertCtrl: AlertController) {

  }

  goToBankPage() {
    this.navCtrl.push(AddBankPage);
  }
  logOut() {
    console.log('this.loggedInUser');
    let alert = this.alertCtrl.create({
      title: 'Logout',
      message: 'Are you sure want to logout?',
      buttons: [
      {
          text: 'Opps, No',
          role: 'cancel',
          handler: () => {
          console.log('Cancel clicked');
          }
      },
      {
          text: 'Yes, Logout',
          handler: () => {
            this.systemService.employeeLogout().subscribe((res) => {
              this.navCtrl.setRoot(LoginPage);
            });
          }
      }
      ]
  });
  alert.present();
  }

}
