import { ErrorHandler, Injectable, Injector } from '@angular/core';

@Injectable()
export class AppErrorHandler implements ErrorHandler {
  // ionicErrorHandler: IonicErrorHandler;

  constructor(injector: Injector) {
  }

  handleError(err: any): void {
    console.warn(err);
    // Remove this if you want to disable Ionic's auto exception handling
    // in development mode.
    // this.ionicErrorHandler && this.ionicErrorHandler.handleError(err);
  }
}
