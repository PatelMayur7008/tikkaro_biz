import { MyAccSubMenuPage } from './myacc-sub-menu/myacc-sub-menu';
import { BizSubMenuPage } from './business-sub-menu/biz-sub-menu';
import { PaymentPage } from './../pages/payment/payment';
declare var Pushy: any;

import { OffersPage } from './../pages/offers/offers';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, App, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { SystemService } from '../services/system-service';
import { CustomerPage } from '../pages/customer/customer';
import { HolidayPage } from '../pages/holiday/holiday';
import { StaffPage } from '../pages/staff/staff';
import { AddBankPage } from '../pages/add-bank/add-bank';
import { GallaryPage } from '../pages/gallary/gallary';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { SettingsPage } from '../pages/settings/settings';
import { BusinessProfilePage } from '../pages/business-profile/business-profile';
import { TimingsPage } from '../pages/timings/timings';
import { ServicesPage } from '../pages/services/services';
import { PackagePage } from '../pages/package/package';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { API } from '../services/api-service';
import { PortfolioPage } from '../pages/portfolio/portfolio';
import { Network } from '@ionic-native/network';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { NoConnectionPage } from '../pages/no-connection-model/no-connection-model';
import { HolidayListPage } from '../pages/holiday-list/holiday-list';
import { AppVersion } from '@ionic-native/app-version';
import { TutorialPage } from '../pages/tutorial/tutorial';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;
  loggedInUser: any = [];
  currentStore: any = '';
  @ViewChild(Nav) nav: Nav;
  pages: any;
  public alertShown: boolean = false;

  constructor(
    public api: API,
    private app: App,
    public platform: Platform,
    public statusBar: StatusBar,
    public storage: Storage,
    public screenOrientation: ScreenOrientation,
    public modalCtrl: ModalController,
    public network: Network,
    private appVersion: AppVersion,
    public systemService: SystemService,
    public splashScreen: SplashScreen,
    public alertCtrl: AlertController) {
    app.viewWillUnload.subscribe(
      () => {
        this.systemService.initializeApplication().subscribe((res) => {
          this.storage.get('isEmployeeLoggedIn').then((res) => {
            console.log('hererer', res);
            if (res == true) {
              this.storage.get('loggedInEmployee').then((res) => {
                this.loggedInUser = res.store_data;
                this.loggedInUser.forEach(element => {
                  if (element.var_title.indexOf(' ') !== -1) {
                    element.shortName = element.var_title.split(" ")[0].charAt(0) + element.var_title.split(" ")[1].charAt(0);
                  } else {
                    element.shortName = element.var_title.split(" ")[0].charAt(0) + element.var_title.split(" ")[0].charAt(1);
                  }
                });
                console.log('this.loggedInUser', this.loggedInUser);
                this.currentStore = res.currentStore;
              })
            }
          })
        })
      }
    )

    this.initializeApp();
    this.pages = [
      { title: 'Dashboard', component: DashboardPage, icon: 'view_compact', isMaterialIcons: true, isSubMenu: false },
      { title: 'Clients', component: CustomerPage, ios: 'md-contacts', md: 'md-contacts', isMaterialIcons: false, isSubMenu: false },
      { title: 'Offers', component: OffersPage, icon: 'card_giftcard', isMaterialIcons: true, isSubMenu: false },
      { title: 'Packages', component: PackagePage, icon: 'card_travel', isMaterialIcons: true, isSubMenu: false },
      { title: 'Gallary', component: GallaryPage, icon: 'collections', isMaterialIcons: true, isSubMenu: false },
      { title: 'Portfolio', component: PortfolioPage, icon: 'burst_mode', isMaterialIcons: true, isSubMenu: false },
      { title: 'Holiday Manager', component: HolidayListPage, icon: 'event', isMaterialIcons: true, isSubMenu: false },
      { title: 'Business Profile', component: BizSubMenuPage, icon: 'business_center', isMaterialIcons: true, isSubMenu: true },
      { title: 'Payment History', component: PaymentPage, icon: 'account_balance_wallet', isMaterialIcons: true, isSubMenu: false },
      { title: 'My Account', component: MyAccSubMenuPage, icon: 'account_circle', isMaterialIcons: true, isSubMenu: true },

      // { title: 'Services', component: ServicesPage, ios: 'ios-settings', md: 'md-settings' },
      // { title: 'Staff Manager', component: StaffPage, ios: 'ios-settings', md: 'md-settings' },

      // { title: 'Bank Details', component: AddBankPage, ios: 'ios-settings', md: 'md-settings' },
      // { title: 'General Settings', component: SettingsPage, ios: 'ios-settings', md: 'md-settings' },
      // { title: 'Opening & Closing Hours', component: TimingsPage, ios: 'ios-settings', md: 'md-settings' },
    ];
  }

  initializeApp() {
    let networkState = this.network.type;
    if (networkState == 'none') {
      let modal = this.modalCtrl.create(NoConnectionPage);
      modal.present();
    }
    this.network.onDisconnect().subscribe(() => {
      let modal = this.modalCtrl.create(NoConnectionPage);
      modal.present();
    });


    let that = this;
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.platform.registerBackButtonAction(() => {
        let nav = this.app.getActiveNav();
        if (nav.canGoBack()) { //Can we go back?
          nav.pop();
        } else {
          this.presentConfirm();
        }
        if (this.alertShown == false) {

        }
      }, 0)
      document.addEventListener('deviceready', function () {
        Pushy.requestStoragePermission();
        Pushy.listen();
      });
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      this.systemService.initializeApplication().subscribe((res) => {
        this.storage.get('isEmployeeLoggedIn').then((res) => {
          console.log('hererer', res);
          if (res == true) {
            this.storage.get('loggedInEmployee').then((res) => {
              this.loggedInUser = res.store_data;
              this.loggedInUser.forEach(element => {
                if (element.var_title.indexOf(' ') !== -1) {
                  element.shortName = element.var_title.split(" ")[0].charAt(0) + element.var_title.split(" ")[1].charAt(0);
                } else {
                  element.shortName = element.var_title.split(" ")[0].charAt(0) + element.var_title.split(" ")[0].charAt(1);
                }
              });
              console.log('this.loggedInUser', this.loggedInUser);
              this.currentStore = res.currentStore;
              document.addEventListener('deviceready', function () {
                Pushy.register(function (err, deviceToken) {
                  if (err) {
                    return alert(err);
                  }
                  // alert(res.data.id + '--' + deviceToken)
                  that.api.sentPushyId({ user_id: res.data.id, pushyId: deviceToken }).subscribe((res) => {
                    console.log(res)
                  })
                });
              });
            })
            this.rootPage = DashboardPage;
          } else {
            this.rootPage = TutorialPage;
          }
        })
      })
    });
  }

  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Exit App',
      message: 'Do you want exit App?',
      buttons: [
        {
          text: 'Opps, No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
            this.alertShown = false;
          }
        },
        {
          text: 'Yes, Exit',
          handler: () => {
            console.log('Yes clicked');
            this.platform.exitApp();
          }
        }
      ]
    });
    alert.present().then(() => {
      this.alertShown = true;
    });
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }
  changeAccount(data) {
    this.storage.get('loggedInEmployee').then((res) => {
      res.currentStore = data;
      this.systemService.setUserdata(res).subscribe((res) => {
        this.initializeApp();
        this.nav.setRoot(DashboardPage);
      });
    });
  }

  logOut() {
    console.log('this.loggedInUser');
    this.systemService.employeeLogout().subscribe((res) => {
      this.nav.setRoot(LoginPage);
    });
  }
}



