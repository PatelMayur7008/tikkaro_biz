import { Component } from '@angular/core';
import { NavController, ViewController, NavParams } from 'ionic-angular';
import * as moment from 'moment';
import { Storage } from '@ionic/storage';
import { BusinessProfilePage } from '../../pages/business-profile/business-profile';
import { ServicesPage } from '../../pages/services/services';
import { StaffPage } from '../../pages/staff/staff';
import { SettingsPage } from '../../pages/settings/settings';
import { TimingsPage } from '../../pages/timings/timings';

@Component({
  selector: 'page-biz-sub-menu',
  templateUrl: 'biz-sub-menu.html'
})
export class BizSubMenuPage {

  constructor(public navCtrl: NavController, public viewCtrl: ViewController, private navParams: NavParams) {

  }

  goToGeneralPage() {
    this.navCtrl.push(BusinessProfilePage);
  }
  goToServicesPage() {
    this.navCtrl.push(ServicesPage);
  }
  goToStaffPage() {
    this.navCtrl.push(StaffPage);
  }
  goToSettingsPage() {
    this.navCtrl.push(SettingsPage);
  }
  goToOpeningHoursPage() {
    this.navCtrl.push(TimingsPage);
  }

}
