import { Component, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { AddHolidayPage } from '../add-holiday/add-holiday';
import { IonicPage, Nav, NavController, ModalController, LoadingController, ToastController, AlertController } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { API } from '../../services/api-service';
import { DashboardPage } from '../dashboard/dashboard';
import { HolidayPage } from '../holiday/holiday';
@Component({
  selector: 'page-holiday-list',
  templateUrl: 'holiday-list.html'
})
export class HolidayListPage {
  loggedInUser: any;
  holidayList: any = [];
  constructor(public navCtrl: NavController,
    public modalCtrl: ModalController,
    private storage: Storage,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private api: API,
  ) {
    this.initializeItems();
  }
  addHoliday() {
    this.navCtrl.push(HolidayPage);
  }

  initializeItems() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.storage.get('loggedInEmployee').then((res) => {
      this.loggedInUser = res;
      this.api.getHolidayList({ store_code: this.loggedInUser.currentStore.var_store_code }).subscribe(data => {
        this.holidayList = JSON.parse(data._body);
        console.log(this.holidayList, 'this.holidayList');
        loading.dismiss();
      }, error => {
        loading.dismiss();
        this.api.showServerError();
        console.log(error);
      })
    })
  }

  editHoliday(holiday: any) {

    let param = {
      id: holiday.id,
      date: holiday.holidayDate,
      date_formatted: holiday.dt_holiday_date,
      timeStarts: holiday.starttime,
      timeEnds: holiday.endtime,
      note: holiday.txt_note,
      isHoliday: (holiday.enum_holiday == 'YES') ? true : false
    }

    this.navCtrl.push(HolidayPage, { editParams: param });

  }

  deleteHoliday(holiday: any) {
    let alert = this.alertCtrl.create({
      title: 'Confirm Delete',
      message: 'Do you want to delete' + holiday.date + '?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Delete',
          handler: () => {
            console.log('holiday', holiday);
            let loading = this.loadingCtrl.create({
              content: 'Please wait...'
            });
            loading.present();
            this.api.deleteHolidays({ holiday_id: holiday.id }).subscribe(data => {
              setTimeout(() => {
                loading.dismiss();
              }, 100);
              let toast = this.toastCtrl.create({
                message: 'Holiday Deleted successfully',
                duration: 1000,
                position: 'bottom'
              });
              toast.present();
              toast.onDidDismiss(() => {
                this.initializeItems();
              });
            }, error => {
              setTimeout(() => {
                loading.dismiss();
              }, 100);
              this.api.showServerError();
              console.log(error);
            })
          }
        }
      ]
    });
    alert.present();
  }
}
