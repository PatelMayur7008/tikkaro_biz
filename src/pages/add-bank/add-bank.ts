import { Component } from '@angular/core';
import { NavController, ViewController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import * as moment from 'moment';
import { Storage } from '@ionic/storage';
import { API } from '../../services/api-service';
import { DashboardPage } from '../dashboard/dashboard';

@Component({
  selector: 'page-add-bank',
  templateUrl: 'add-bank.html'
})
export class AddBankPage {
  loggedInUser: any;
  bankData: any;
  public submitClicked: boolean = false;

  public event = {
    acc_no: '',
    ifsc_code: '',
    name: '',
    bank_name: '',
    branch_name: '',
    address: '',
    acc_type: '',
    card_type: '',
    card_number: '',
    store_id: ''
  }
  constructor(public navCtrl: NavController,
    public viewCtrl: ViewController,
    private storage: Storage,
    private navParams: NavParams,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private api: API,
  ) {
    this.storage.get('loggedInEmployee').then((res) => {
      this.loggedInUser = res;

    })

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.storage.get('loggedInEmployee').then((res) => {
      this.loggedInUser = res;
      this.api.getBankDetails({ store_code: this.loggedInUser.currentStore.var_store_code }).subscribe(data => {
        this.bankData = JSON.parse(data._body);
        console.log(this.bankData.data);
        if (this.bankData.data.length > 0) {
          this.event.acc_no = this.bankData.data[0].var_accountno;
          this.event.ifsc_code = this.bankData.data[0].var_ifsc_code;
          this.event.name = this.bankData.data[0].var_name;
          this.event.bank_name = this.bankData.data[0].var_bank_name;
          this.event.branch_name = this.bankData.data[0].var_branch_name;
          this.event.address = this.bankData.data[0].var_accountno;
          this.event.acc_type = this.bankData.data[0].var_accountno;
          this.event.card_type = this.bankData.data[0].var_debit_card_type;
          this.event.card_number = this.bankData.data[0].var_card_number;
        }
        loading.dismiss();
      }, error => {
        loading.dismiss();
        console.log(error);
        this.api.showServerError();
      })
    })
  }

  addBank() {
    this.submitClicked = true;
    if (this.event.acc_no != '' && this.event.ifsc_code != '' && this.event.name != '' && this.event.bank_name != '' && this.event.branch_name != '') {
      this.event.store_id = this.loggedInUser.currentStore.var_store_code;
      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
      loading.present();
      this.api.addBank(this.event).subscribe(res => {
        loading.dismiss();
        let toast = this.toastCtrl.create({
          message: 'Bank Detail Added successfully',
          duration: 2000,
          position: 'bottom'
        });
        toast.present();
        toast.onDidDismiss(() => {
          // this.navCtrl.setRoot(DashboardPage);
        });
        // if (response.status == 'success') {
        //   this.navCtrl.setRoot(DashboardPage);
        // }
      }, error => {
        loading.dismiss();
        console.log(error);
        this.api.showServerError();        
      })
    }

  }

}
