import { Component } from '@angular/core';
import { NavController, ViewController, NavParams, ToastController, LoadingController, ActionSheetController } from 'ionic-angular';
import * as moment from 'moment';
import { Storage } from '@ionic/storage';
import { API } from '../../services/api-service';
import { DashboardPage } from '../dashboard/dashboard';
import { utilService } from '../../services/core/util-service';
import { BizSubMenuPage } from '../../app/business-sub-menu/biz-sub-menu';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {
  loggedInUser: any;
  weekoff: any;
  saloonFor: any;
  settings: any;

  public event = {
    availableSeat: '',
    fb_link: '',
    instagram_link: '',
    twitter_link: '',
    whatsApp_num: '',
    weekOff: ''
  }
  weekoffArray: any = [
    {
      day: 'Sunday',
      disabled: false,
      checked: false
    },
    {
      day: 'Monday',
      disabled: false,
      checked: false

    },
    {
      day: 'Tuesday',
      disabled: false,
      checked: false

    },
    {
      day: 'Wednesday',
      disabled: false,
      checked: false

    },
    {
      day: 'Thursday',
      disabled: false,
      checked: false

    },
    {
      day: 'Friday',
      disabled: false,
      checked: false

    },
    {
      day: 'Saturday',
      disabled: false,
      checked: false

    }
  ];
  constructor(public navCtrl: NavController,
    public viewCtrl: ViewController,
    private storage: Storage,
    private navParams: NavParams,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private api: API,
    private util: utilService,
    public actionSheetCtrl: ActionSheetController,
  ) {
    // this.storage.get('loggedInEmployee').then((res) => {
    //   this.loggedInUser = res;
    //   console.log('datasdssssss', res);
    // })
    this.getSettings();
    this.util.getDayNameFromNumber(3);
    this.util.getNumberFromDayName('Tuesday');
  }

  selectCategory() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Store Category',
      buttons: [
        {
          text: 'Male',
          handler: () => {
            this.saloonFor = 'MEN'
          }
        },
        {
          text: 'Female',
          handler: () => {
            this.saloonFor = 'WOMEN'
          }
        },
        {
          text: 'Both',
          handler: () => {
            this.saloonFor = 'BOTH'
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            //console.log('Cancel clicked');
          }
        }
      ]
    });
 
    actionSheet.present();
  }
  
  selectNoSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select No Of Seat Available',
      buttons: [
        {
          text: '1',
          handler: () => {
            this.event.availableSeat = '1'
          }
        },
        {
          text: '2',
          handler: () => {
            this.event.availableSeat = '2'
          }
        },
        {
          text: '3',
          handler: () => {
            this.event.availableSeat = '3'
          }
        },
        {
          text: '4',
          handler: () => {
            this.event.availableSeat = '4'
          }
        },
        {
          text: '5',
          handler: () => {
            this.event.availableSeat = '5'
          }
        },
        {
          text: '6',
          handler: () => {
            this.event.availableSeat = '6'
          }
        },
        {
          text: '7',
          handler: () => {
            this.event.availableSeat = '7'
          }
        },
        {
          text: '8',
          handler: () => {
            this.event.availableSeat = '8'
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            //console.log('Cancel clicked');
          }
        }
      ]
    });
 
    actionSheet.present();
  }

  selectWeekOff() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select WeekOff Day',
      buttons: [
        {
          text: 'Sunday',
          handler: () => {
            this.weekoff = 'Sunday',
            this.event.weekOff = 'Sunday'
          }
        },
        {
          text: 'Monday',
          handler: () => {
            this.weekoff = 'Monday'
            this.event.weekOff = 'Monday'
          }
    
        },
        {
          text: 'Tuesday',
          handler: () => {
            this.weekoff = 'Tuesday'
            this.event.weekOff = 'Tuesday'
          }
    
        },
        {
          text: 'Wednesday',
          handler: () => {
            this.weekoff = 'Wednesday'
            this.event.weekOff = 'Wednesday'
          }
    
        },
        {
          text: 'Thursday',
          handler: () => {
            this.weekoff = 'Thursday'
            this.event.weekOff = 'Thursday'
          }
    
        },
        {
          text: 'Friday',
          handler: () => {
            this.weekoff = 'Friday'
            this.event.weekOff = 'Friday'
          }
    
        },
        {
          text: 'Saturday',
          handler: () => {
            this.weekoff = 'Saturday'
            this.event.weekOff = 'Saturday'
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            //console.log('Cancel clicked');
          }
        }
      ]
    });
 
    actionSheet.present();
  }
  
  getSettings() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.storage.get('loggedInEmployee').then((res) => {
      this.loggedInUser = res;
      this.api.getSettings({ store_code: this.loggedInUser.currentStore.var_store_code }).subscribe(res => {
        loading.dismiss();
        this.settings = JSON.parse(res._body).data;
        console.log(this.settings)
        this.event.availableSeat = this.settings.int_no_book_seats;
        this.event.fb_link = this.settings.var_fb_link;
        this.event.instagram_link = this.settings.var_instagram_link;
        this.event.twitter_link = this.settings.var_twitter_link;
        if(this.settings.var_whatsapp_no == '' || this.settings.var_whatsapp_no ==null){
          this.event.whatsApp_num = this.loggedInUser.currentStore.bint_phone;
        }else{
          this.event.whatsApp_num = this.settings.var_whatsapp_no;
        }
        this.saloonFor = this.settings.enum_store_type;
        this.weekoffArray.forEach(element => {
          if (this.util.getDayNameFromNumber(parseInt(this.settings.int_weekoff)) == element.day) {
            this.weekoff = element.day;
            this.event.weekOff = element.day;
            element.checked = true
          } else {
            element.checked = false
          }
        });
        // this.weekoff = this.util.getDayNameFromNumber(parseInt(this.settings.int_weekoff));

      }, error => {
        loading.dismiss();
        this.api.showServerError();
        console.log(error);
      })
    })
  }

  updateSettings() {
    // this.event.store_id = this.loggedInUser.data.var_store_code;
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    let payload = {
      store_code: this.loggedInUser.currentStore.var_store_code,
      int_no_book_seats: this.event.availableSeat,
      var_whatsapp_no: this.event.whatsApp_num,
      var_twitter_link: this.event.twitter_link,
      var_fb_link: this.event.fb_link,
      var_instagram_link: this.event.instagram_link,
      enum_store_type: this.saloonFor,
      int_weekoff: this.util.getNumberFromDayName(this.weekoff)
    }

    this.api.updateSettings(payload).subscribe(res => {
      loading.dismiss();
      this.navCtrl.setRoot(BizSubMenuPage);
      this.api.getToastMessage('Store settings updated successfully');
    }, error => {
      loading.dismiss();
      this.api.showServerError();
      console.log(error);
    })
  }

  checked(day) {
    this.weekoff = day;
    this.weekoffArray.forEach(week => {
      if (day.day != week.day) {
        week.checked = false
      }
    });
    if (day.checked) {
      this.weekoffArray.forEach(week => {
        if (day.day != week.day) {
          week.disabled = true
        }
      });
    } else {
      this.weekoffArray.forEach(week => {
        if (day.day != week.day) {
          week.disabled = false
        }
      });
    }
  }
}
