import { Component } from '@angular/core';
import { NavController, ViewController, NavParams, AlertController, LoadingController, ToastController } from 'ionic-angular';
import * as moment from 'moment';
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker';
import { API } from '../../../services/api-service';

@Component({
  selector: 'page-add-staff',
  templateUrl: 'add-staff.html'
})
export class AddStaffPage {
  public submitClicked: boolean = false;
  public currentYear : string = moment().format('YYYY-MM');
  public event = {
    name: '',
    phone: '',
    startDate: moment().format('YYYY-MM-DD'),
    endDate: moment(new Date()).format('YYYY-MM-DD'),
    isFrontView: false,
    active: false,
    current: false,
    isAdd: false,
    image: '',
    imageFirst: '',
    id: ''
  }
  constructor(public navCtrl: NavController,
    public viewCtrl: ViewController,
    private api: API,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private navParams: NavParams,
    private imagePicker: ImagePicker
  ) {
    console.log(this.currentYear)
    if (this.navParams.data.staff) {
      console.log('data', this.navParams.data.staff);
      this.event.name = this.navParams.data.staff.var_name
      this.event.phone = this.navParams.data.staff.var_phone
      this.event.startDate = moment(this.navParams.data.staff.dt_startdate).format('YYYY-MM-DD')
      this.event.endDate = (this.navParams.data.staff.dt_enddate ? moment(this.navParams.data.staff.dt_enddate).format('YYYY-MM-DD') : moment().format('YYYY-MM-DD'))
      this.event.isFrontView = (this.navParams.data.staff.enum_isFrontView == 'YES' ? true : false)
      this.event.current = (this.navParams.data.staff.enum_current == 'YES' ? true : false)
      this.event.active = (this.navParams.data.staff.enum_active == 'YES' ? true : false)
      this.event.image = this.navParams.data.staff.var_image;
      this.event.id = this.navParams.data.staff.id;
      this.event.imageFirst = this.event.image.charAt(0);
    } else {
      this.event.isAdd = true;
      console.log('datasdssssss');
    }
  }

  addStaff() {
    this.submitClicked = true;
    // return false;
    if (this.event.name != '') {
      this.viewCtrl.dismiss({ data: this.event, other: this.navParams.data.staff });
    }
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  selectImage() {
    let options: ImagePickerOptions = {
      maximumImagesCount: 1
    };
    this.imagePicker.getPictures(options).then((results) => {
      for (var i = 0; i < results.length; i++) {
        this.event.image = results[i];
        this.event.imageFirst = this.event.image.charAt(0);
      }
    }, (err) => { });
  }

  deleteStaff(user) {
    console.log('user', user);
    let alert = this.alertCtrl.create({
      title: 'Remove Staff Member',
      message: `Do you want to remove staff member "${user.name}?"`,
      buttons: [
        {
          text: 'Oops, No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes, Remove',
          handler: () => {
            console.log('user', user);
            let loading = this.loadingCtrl.create({
              content: 'Please wait...'
            });
            loading.present();
            this.api.deleteStaff({ staff_id: user.id }).subscribe(data => {
              loading.dismiss();
              this.viewCtrl.dismiss();
              this.api.getToastMessage(`Staff member "${user.name}" removed successfully`);
            }, error => {
              loading.dismiss();
              this.api.showServerError();
              console.log(error);
            })
          }
        }
      ]
    });
    alert.present();
  }

}
