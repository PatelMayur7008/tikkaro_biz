import * as moment from 'moment';
import { AddStaffPage } from './add-staff/add-staff';
import { Component, ViewChild } from '@angular/core';
import { AddHolidayPage } from '../add-holiday/add-holiday';
import { IonicPage, Nav, NavController, ModalController, LoadingController, ToastController, AlertController } from 'ionic-angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';

import { Storage } from '@ionic/storage';
import { API } from '../../services/api-service';
import { DashboardPage } from '../dashboard/dashboard';
import { API_BASE_URL } from '../../services/core/constants';
import { utilService } from '../../services/core/util-service';

@Component({
  selector: 'page-staff',
  templateUrl: 'staff.html'
})
export class StaffPage {
  staff: any = [];
  loggedInUser: any;
  staff1: any;
  fileTransfer: FileTransferObject;

  constructor(public navCtrl: NavController,
    public modalCtrl: ModalController,
    private storage: Storage,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private api: API,
    private utilService: utilService,
    private transfer: FileTransfer,
    private alertCtrl: AlertController

  ) {
    this.initializeItems();
    this.fileTransfer = this.transfer.create();

  }

  selectUser(user) {
    let modal = this.modalCtrl.create(AddStaffPage, { staff: user });
    modal.present();
    modal.onDidDismiss(data => {
      console.log('data', data);
      if (data) {
        console.log(data.data)
        if (data.data.imageFirst == 'h') {
          let loading = this.loadingCtrl.create({
            content: 'Please wait...'
          });
          loading.present();
          let payload = {
            staff_id: (data.other ? data.other.id : ''),
            name: data.data.name,
            phone: data.data.phone,
            start_date: data.data.startDate,
            end_date: data.data.endDate,
            enum_frontview: (data.data.isFrontView == false ? 'NO' : 'YES'),
            enum_active: (data.data.active == false ? 'NO' : 'YES'),
            enum_current: (data.data.current == false ? 'NO' : 'YES')
          }
          this.api.updateStaff(payload).subscribe(data => {
            loading.dismiss();
            this.initializeItems();
            console.log('-0-----------')
            console.log(data)
            let res = JSON.parse(data._body);
            let toast = this.toastCtrl.create({
              message: res.message,
              duration: 2000,
              position: 'bottom'
            });
            toast.present();
            toast.onDidDismiss(() => {
            });
          }, error => {
            loading.dismiss();
            this.api.showServerError();
            console.log(error);
          })
        } else {
          console.log('files');
          let loader = this.loadingCtrl.create({
            content: "Uploading..."
          });
          loader.present();
          let options: FileUploadOptions = {
            fileKey: 'file',
            fileName: `receipt_pic.jpg`,
            httpMethod: 'POST',
            mimeType: 'image/jpg',
            chunkedMode: false,
            params: {
              staff_id: (data.other ? data.other.id : ''),
              name: data.data.name,
              phone: data.data.phone,
              start_date: data.data.startDate,
              end_date: data.data.endDate,
              enum_frontview: (data.data.isFrontView == false ? 'NO' : 'YES'),
              enum_active: (data.data.active == false ? 'NO' : 'YES'),
              enum_current: (data.data.current == false ? 'NO' : 'YES')
            },
            headers: {
              "Accept": "image/jpg",
              'Authorization': 'bearer ' + this.utilService.getToken(),
            }
          };
          console.log(data.data.image)
          console.log(options.params)
          // this.fileTransfer.upload(data.data.image, 'http://519f3288.ngrok.io/tikkaro/apibiz/', options)
          this.fileTransfer.upload(data.data.image, API_BASE_URL + '/updateStoreStaffWithImage', options)
            .then((data) => {
              // alert("success" + JSON.stringify(data));
              console.log(JSON.stringify(data));
              console.log(JSON.parse(JSON.stringify(data)));
              let res = JSON.parse(JSON.parse(JSON.stringify(data)).response);

              loader.dismiss();
              this.initializeItems();
              let toast = this.toastCtrl.create({
                message: res.message,
                duration: 2000,
                position: 'bottom'
              });
              toast.present();
              toast.onDidDismiss(() => {
              });
            }, (err) => {
              console.log('error', err);
              console.log(JSON.stringify(err));
              loader.dismiss();
            });
        }

      } else {
        this.initializeItems();
      }

    });
  }

  deleteStaff(user) {
    console.log(user);
    let alert = this.alertCtrl.create({
      title: 'Remove Staff Member',
      message: `Do you want to remove staff member "${user.var_name}?"`,
      buttons: [
        {
          text: 'Oops, No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes, Remove',
          handler: () => {
            console.log('user', user);
            let loading = this.loadingCtrl.create({
              content: 'Please wait...'
            });
            loading.present();
            this.api.deleteStaff({ staff_id: user.id }).subscribe(data => {
              loading.dismiss();
              this.initializeItems();
              this.api.getToastMessage(`Staff member "${user.var_name}" removed successfully`);
            }, error => {
              loading.dismiss();
              this.api.showServerError();
              console.log(error);
            })
          }
        }
      ]
    });
    alert.present();
  }

  initializeItems() {
    // this.staff = [
    //   {
    //     id: 1,
    //     isFrontView: true,
    //     status: 'Active',
    //     startDate: '2018-01-04',
    //     endDate: '2018-01-04',
    //     name: 'nirav',
    //     phone: '9874553210',
    //     current: true
    //   },
    //   {
    //     id: 2,
    //     isFrontView: true,
    //     status: 'Active',
    //     startDate: '2018-01-04',
    //     endDate: '2018-01-04',
    //     name: 'mayur',
    //     phone: '9874553210',
    //     current: true
    //   },
    //   {
    //     id: 3,
    //     isFrontView: true,
    //     status: 'Not Active',
    //     startDate: '2018-01-04',
    //     endDate: '2018-01-04',
    //     name: 'jayesh',
    //     phone: '9874553210',
    //     current: false

    //   }
    // ];

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.storage.get('loggedInEmployee').then((res) => {
      this.loggedInUser = res;
      console.log('this.loggedInUser', this.loggedInUser);

      this.api.getStaffList({ store_code: this.loggedInUser.currentStore.var_store_code }).subscribe(data => {
        this.staff1 = JSON.parse(data._body);
        console.log(this.staff1);
        if (typeof this.staff1 !== 'undefined') {
          this.staff1.forEach(element => {
            if (element.dt_startdate) {
              element.dt_startdate = moment(element.dt_startdate).format('DD MMM YYYY');
              element.dt_enddate = moment(element.dt_enddate).format('DD MMM YYYY');
            }
            setTimeout(() => {
              element.var_image_thumb = element.var_image;
            }, 500);
          });
        }
        loading.dismiss();
        this.filterItem();
      }, error => {
        loading.dismiss();
        this.api.showServerError();
        console.log(error);
      })
    })
  }

  filterItem() {
    this.staff = this.staff1;
    console.log(this.staff,'sadas dasd ');
  }

  getItems(ev) {
    this.filterItem();
    var val = ev.target.value;
    console.log(val)
    console.log(this.staff)
    if (val && val.trim() != '') {
      this.staff = this.staff.filter((item) => {
        return ((item.var_name.toLowerCase().indexOf(val.toLowerCase()) > -1) ||
          (item.var_phone.toLowerCase().indexOf(val.toLowerCase()) > -1));
      })
    }
  }

  addStaff() {
    let that = this;
    let modal = this.modalCtrl.create(AddStaffPage, { staff: '' });
    modal.present();
    modal.onDidDismiss(data => {
      console.log(data);
      if (data) {
        console.log(data.data.imageFirst)
        if (data.data.imageFirst == '') {
          let payload = {
            store_id: this.loggedInUser.currentStore.var_store_code,
            name: data.data.name,
            phone: data.data.phone,
            start_date: data.data.startDate,
            end_date: data.data.endDate,
            enum_frontview: (data.data.isFrontView == false ? 'NO' : 'YES'),
            enum_active: (data.data.active == false ? 'NO' : 'YES'),
            enum_current: (data.data.current == false ? 'NO' : 'YES')
          }
          let loading = this.loadingCtrl.create({
            content: 'Please wait...'
          });
          loading.present();
          this.api.addStaff(payload).subscribe(data => {
            loading.dismiss();
            this.initializeItems();
            let res = JSON.parse(data._body);
            let toast = this.toastCtrl.create({
              message: res.message,
              duration: 2000,
              position: 'bottom'
            });
            toast.present();
            toast.onDidDismiss(() => {
              // this.navCtrl.setRoot(DashboardPage);
            });
          }, error => {
            loading.dismiss();
            this.api.showServerError();
            console.log(error);
          })
        } else {
          console.log('files');
          let loader = this.loadingCtrl.create({
            content: "Uploading..."
          });
          loader.present();
          let options: FileUploadOptions = {
            fileKey: 'file',
            fileName: `receipt_pic.jpg`,
            httpMethod: 'POST',
            mimeType: 'image/jpg',
            chunkedMode: false,
            params: {
              store_id: this.loggedInUser.currentStore.var_store_code,
              name: data.data.name,
              phone: data.data.phone,
              start_date: data.data.startDate,
              end_date: data.data.endDate,
              enum_frontview: (data.data.isFrontView == false ? 'NO' : 'YES'),
              enum_active: (data.data.active == false ? 'NO' : 'YES'),
              enum_current: (data.data.current == false ? 'NO' : 'YES')
            },
            headers: {
              "Accept": "image/jpg",
              'Authorization': 'bearer ' + this.utilService.getToken()
            }
          };
          console.log(data.data.image)
          let img = data.data.image;
          console.log(img)
          console.log(typeof (img))

          that.fileTransfer.upload(img, API_BASE_URL + '/addStoreStaffWithImage', options)
            .then((data) => {
              // alert("success" + JSON.stringify(data));
              // console.log(JSON.stringify(data));
              // console.log(JSON.parse(JSON.stringify(data)));
              // let res = JSON.parse(JSON.parse(JSON.stringify(data)).response);
              console.log('error', data.response);
              let res = JSON.parse(data.response);
              loader.dismiss();
              this.initializeItems();
              let toast = this.toastCtrl.create({
                message: res.message,
                duration: 2000,
                position: 'bottom'
              });
              toast.present();
              toast.onDidDismiss(() => {
              });
            }, (err) => {
              console.log('error', err);
              console.log('error', err.response);
              let res = JSON.parse(err.response);
              // let res = JSON.parse(JSON.parse(JSON.stringify(err)).body);
              loader.dismiss();
              let toast = this.toastCtrl.create({
                message: res.message,
                duration: 2000,
                position: 'bottom'
              });
              toast.present();
              this.initializeItems();
            });
        }


      }
    });
  }
}
