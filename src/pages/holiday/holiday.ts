import { Component, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { AddHolidayPage } from '../add-holiday/add-holiday';
import { IonicPage, Nav, NavController, ModalController, LoadingController, ToastController, AlertController } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { API } from '../../services/api-service';
import { DashboardPage } from '../dashboard/dashboard';
import { HolidayListPage } from '../holiday-list/holiday-list';
import { NavParams } from '../../../node_modules/ionic-angular/navigation/nav-params';
@Component({
  selector: 'page-holiday',
  templateUrl: 'holiday.html'
})
export class HolidayPage {
  public disabled: boolean = false;
  public checkedOrNot: boolean = false;
  loggedInUser: any;
  isedit:boolean = false;
  editDate:any = false;
  pastDate = moment();
  type: 'string';
  _daysConfig: any = [{
    subTitle: `$`
  }];
  optionsMulti;
  // optionsMulti = {
  //   from: moment(this.pastDate),
  //   // defaultSubtitle: 'eee'
  //   date: moment(),
  //   daysConfig: this._daysConfig
  // };
  public event = {
    id: 0,
    date: moment(),
    timeStarts: moment().format('HH:mm'),
    timeEnds: moment().format('HH:mm'),
    note: '',
    isHoliday: true
  }
  storeSettings: any;
  TableArry: any = [];

  constructor(public navCtrl: NavController,
    public modalCtrl: ModalController,
    private storage: Storage,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private api: API,
    private navParams: NavParams,
    private alertCtrl: AlertController,
  ) {
    // this.navParams.data.editParams
    // console.log(this.event.timeStarts,'this.event.timeStarts');

    this.getHolidayList();
    // For Edit Holiday 

    // if (this.navParams.data.editParams) {
    //   this.event = this.navParams.data.editParams;
    //   console.log('Edit TIme');
    //   this.editDate = this.event.date;
    //   this.isedit = true;
    // } else {
    //   this.isedit = false;
    //   console.log('Add Time');
    // }

    this.storage.get('loggedInEmployee').then((res) => {
      this.loggedInUser = res;
      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });

      // loading.present();
      this.api.getStoreTiming({ store_code: this.loggedInUser.currentStore.var_store_code, date: this.event.date }).subscribe(data => {
        // loading.dismiss();
        console.log(JSON.parse(data._body).data.openingTime);
        this.event.timeStarts = JSON.parse(data._body).data.openingTime;
        this.event.timeEnds = JSON.parse(data._body).data.closingTime;
        console.log(JSON.parse(data._body));


      }, error => {
        setTimeout(() => {
          loading.dismiss();
        }, 100);
        this.api.showServerError();
        console.log(error);
      })
    });


  }

  deleteHoliday(holiday) {

    let alert = this.alertCtrl.create({
      title: 'Complete Booking',
      message: `Do you want to remove holiday on ${holiday.date_formatted}?`,
      buttons: [
        {
          text: 'Opps, No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes, Remove',
          handler: () => {
            let loading = this.loadingCtrl.create({
              content: 'Please wait...'
            });
            loading.present();
            this.api.deleteHolidays({ holiday_id: holiday.id }).subscribe(data => {
              setTimeout(() => {
                loading.dismiss();
              }, 100);
              //this.getHolidayList();
              this.api.getToastMessage(`Holiday of ${holiday.date_formatted} removed successfully`);
              this.navCtrl.setRoot(HolidayListPage);
            }, error => {
              setTimeout(() => {
                loading.dismiss();
              }, 100);
              this.api.showServerError();
              console.log(error);
            })
          }
        }
      ]
    });
    alert.present();
    // var removeIndex = this.TableArry.map(function (item) { return item.id; }).indexOf(37);
    // this.TableArry.splice(removeIndex, 1);
  }

  getHolidayList() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.storage.get('loggedInEmployee').then((res) => {
      this.loggedInUser = res;
      console.log('this.loggedInUser', this.loggedInUser);

      this.api.getSettings({ store_code: this.loggedInUser.currentStore.var_store_code }).subscribe(getSettingsdata => {
        this.storeSettings = JSON.parse(getSettingsdata._body).data;
        console.log(this.storeSettings);
        this.api.getHolidays({ store_code: this.loggedInUser.currentStore.var_store_code }).subscribe(data => {
          this.TableArry = JSON.parse(data._body).data;
          // this.selectDate(moment());
          let arrrayy = [];
          this.TableArry.forEach(element => {
            if (element.enum_holiday == 'YES') {
              arrrayy.push({
                date: element.dt_holiday_date,
                // subTitle: `$`
                cssClass: "active"
              })
            }
          });
          console.log(arrrayy);
          this.optionsMulti = {
            from: moment(this.pastDate),
            daysConfig: arrrayy,
            disableWeeks: [parseInt(this.storeSettings.int_weekoff)]
          };

           // For Edit Holiday 
          if (this.navParams.data.editParams) {
            this.event = this.navParams.data.editParams;
            this.editDate = this.event.date;
            this.isedit = true;
          } else {
            this.isedit = false;
          }

          setTimeout(() => {
            loading.dismiss();
          }, 100);
          // console.log('this.TableArry', this.TableArry);


        }, error => {
          setTimeout(() => {
            loading.dismiss();
          }, 100);
          this.api.showServerError();
          console.log(error);
        })
      });


    })
  }

  selectDate(e) {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.api.getStoreTiming({ store_code: this.loggedInUser.currentStore.var_store_code, date: e }).subscribe(data => {
      setTimeout(() => {
        loading.dismiss();
      }, 100);
      console.log(JSON.parse(data._body).data.openingTime);
      this.event.timeStarts = JSON.parse(data._body).data.openingTime;
      this.event.timeEnds = JSON.parse(data._body).data.closingTime;
    }, error => {
      setTimeout(() => {
        loading.dismiss();
      }, 100);
      this.api.showServerError();
      console.log(error);
    })

    let recored
    // console.log('iffff', recored);

    if (this.TableArry.length > 0) {
      recored = this.TableArry.filter(cat => moment(cat.dt_holiday_date).format('DD MMMM YYYY') == moment(e).format('DD MMMM YYYY'))[0];
    }
    // console.log('iffff', recored);
    if (recored && moment(recored.dt_holiday_date).format('DD MMMM YYYY') == moment(e).format('DD MMMM YYYY')) {
      // console.log('iffff', recored);
      this.event.timeStarts = recored.dt_start_time;
      this.event.timeEnds = recored.dt_end_time;
      this.event.note = recored.txt_note;
      this.event.isHoliday = (recored.enum_holiday == 'NO' ? false : true);
    } else {
      // console.log('iffff', recored);
      this.event.timeStarts = moment().format('HH:mm');
      this.event.timeEnds = moment().format('HH:mm');
      this.event.note = '';
      this.event.isHoliday = true;
    }
    // this.TableArry.forEach(element => {
    //   if (element.isToday == true) {
    //     console.log('iffff');
    //     this.event.timeStarts = element.dt_start_time;
    //     this.event.timeEnds = element.dt_end_time;
    //     this.event.note = element.txt_note;
    //     this.event.isHoliday = (element.enum_holiday == 'NO' ? false : true);
    //   } else {
    //     console.log('elseee');
    //     this.event = {
    //       date: moment().format('YYYY-MM-DD'),
    //       timeStarts: moment().format('HH:mm'),
    //       timeEnds: moment().format('HH:mm'),
    //       note: '',
    //       isHoliday: false
    //     }
    //   }
    // });
    // console.log(this.event);

  }
  // checked(day) {
  //   if (day.checked) {
  //     this.weekoffArray.forEach(week => {
  //       if (day.day != week.day) {
  //         week.disabled = true
  //       }
  //     });
  //   } else {
  //     this.weekoffArray.forEach(week => {
  //       if (day.day != week.day) {
  //         week.disabled = false
  //       }
  //     });
  //   }
  // }

  addHoliday() {
    console.log(this.loggedInUser);
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    let payload = {
      store_id: this.loggedInUser.currentStore.var_store_code,
      holiday_date: moment(this.event.date).format('DD-MM-YYYY'),
      date_formatted: moment(this.event.date).format('DD-MMMM-YYYY'),
      start_time: this.event.timeStarts,
      end_time: this.event.timeEnds,
      holiday_note: this.event.note,
      enum_holiday: this.event.isHoliday
    }
    this.api.addHolidays(payload).subscribe(data => {

      setTimeout(() => {
        loading.dismiss();
      }, 100);
      // this.getHolidayList();
      if(this.isedit){
        var message = `Holiday Of ${payload.date_formatted} Updated successfully`;
      }else{
        var message = `Holiday Of ${payload.date_formatted} Addedd successfully`;
      }
      
      this.api.getToastMessage(message);
      this.navCtrl.setRoot(HolidayListPage);
      // toast.onDidDismiss(() => {
      //   this.navCtrl.setRoot(HolidayListPage);
      // });
    }, error => {
      setTimeout(() => {
        loading.dismiss();
      }, 100);
      this.api.showServerError();
      console.log(error);
    })
    // this.TableArry.push({
    //   id: Math.random(),
    //   date: data.date,
    //   fromTime: data.timeStarts,
    //   toTime: data.timeEnds,
    //   note: data.text,
    //   action: 'Delete'
    // });
  }
}
