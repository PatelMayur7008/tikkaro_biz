import { Component } from '@angular/core';
import * as moment from 'moment';
import { IonicPage, ViewController, NavParams, Nav, NavController, ModalController, LoadingController, ToastController, AlertController } from 'ionic-angular';
import { API } from '../../../services/api-service';
import { Storage } from '@ionic/storage';
import { AddoffersPage } from './../add-offers/add-offers';
import { ServicesPage } from '../../services/services';
import { AddServicesPage } from '../../services/add-services/add-services';

@Component({
  selector: 'page-offers-detail',
  templateUrl: 'offers-detail.html'
})
export class OffersDetailPage {
  data;
  loggedInUser: any;
  userDetails;
  storeServices: any = [];
  packages;
  public submitClicked: boolean = false;
  minDate = moment().format('YYYY-MM-DD');
  // maxDate  = moment('01-01-2050').format('YYYY-MM-DD');
  maxDate = moment().add(3, 'y').format('YYYY-MM-DD');
  public event = {
    name: '',
    totalPrice: 0,
    oldtotalPrice: 0,
    note: '',
    startDate: moment().format('YYYY-MM-DD'),
    endDate: moment().format('YYYY-MM-DD'),
    isNew: false,
    enum_enable: true
  }
  constructor(public navCtrl: NavController,
    public viewCtrl: ViewController,
    private navParams: NavParams,
    private toastCtrl: ToastController,
    public modalCtrl: ModalController,
    private alertCtrl: AlertController,
    private storage: Storage,
    public loadingCtrl: LoadingController,
    private api: API,

  ) {

    this.storage.get('loggedInEmployee').then((res) => {
      this.loggedInUser = res;
      this.api.getStoreService({ store_code: this.loggedInUser.currentStore.var_store_code }).subscribe(data => {
        console.log(JSON.parse(data._body).data);
        this.storeServices = JSON.parse(data._body).data;
        console.log('this.storeServices', this.storeServices);
      });
    });

    if (this.navParams.get('package')) {
      this.data = this.navParams.get('package');
      console.log(this.data,'PARAM DATA');
      this.event.name = this.data.var_name;
      this.event.enum_enable = (this.data.enum_enable == 'YES' ? true : false);
      this.event.endDate = moment(this.data.dt_end_date).format('YYYY-MM-DD');
      this.event.startDate = moment(this.data.dt_start_date).format('YYYY-MM-DD');
      this.event.note = this.data.txt_note;
      this.packages = this.data.services;
      if (this.packages.length > 0) {
        this.event.totalPrice = 0;
        this.event.oldtotalPrice = 0;
        this.packages.forEach(element => {
          this.event.totalPrice += parseInt(element.var_offer_price);
          this.event.oldtotalPrice += parseInt(element.var_price);

        });
      }
    } else {
      this.packages = [];
      this.event.isNew = true;
    }
    console.log('this.data', this.data);
    console.log('this.event', this.event);

  }

  goToeditPackage(c) {
    let modal = this.modalCtrl.create(AddoffersPage, { package: c });
    modal.present();
    modal.onDidDismiss(data => {
      console.log(data);
      if (data.data) {
        if (data.data.isAdd == false) {
          this.packages.forEach(element => {
            if (data.data.id == element.id) {
              element.id = data.data.id;
              element.var_offer_price = data.data.price;
              element.var_price = data.data.oldPrice;
              element.var_title = data.data.name;
            }
          });
        } else {
          this.packages.push({
            id: data.data.id,
            var_offer_price: data.data.price,
            var_price: data.data.oldPrice,
            var_title: data.data.name
          })
        }
      }
      console.log(this.data);
    });
  }

  goToaddPackage() {
    // console.log(this.packages,'this.searvices')
    if (this.storeServices.length > 0) {
      let modal = this.modalCtrl.create(AddoffersPage, { already: this.packages });
      modal.present();
      modal.onDidDismiss(data => {
        console.log('ffff', data);
        if (data.data) {
          if (data.data.isAdd == false) {
            console.log('ifff')
            this.packages.forEach(element => {
              if (data.data.id == element.id) {
                element.id = data.data.id;
                element.var_offer_price = data.data.price;
                element.var_price = data.data.oldPrice;
                element.var_title = data.data.name;
              }
            });
          } else {
            console.log('else')
            // if (data.data.isNew == true) {
            this.packages.push({
              id: data.data.id,
              var_offer_price: data.data.price,
              var_price: data.data.oldPrice,
              var_title: data.data.name,
              is_new: true
            })
            // }else{
            // this.packages.push({
            //   id: data.data.id,
            //   var_offer_price: data.data.price,
            //   var_title: data.data.name,
            //   is_new: false
            // })
            // }
          }
        }

        if (this.packages.length > 0) {
          this.event.totalPrice = 0;
          this.event.oldtotalPrice = 0;
          this.packages.forEach(element => {
            this.event.totalPrice += parseInt(element.var_offer_price);
            this.event.oldtotalPrice += parseInt(element.var_price);
          });
        }
        console.log(this.packages)
        console.log(this.event)
      });
    } else {
      let toast = this.toastCtrl.create({
        message: 'Please atleast one service add in your store.',
        duration: 1000,
        position: 'bottom'
      });
      toast.present();
      toast.onDidDismiss(() => {
        this.navCtrl.setRoot(AddServicesPage);
      });
    }

  }

  deleteService(s) {
    console.log(s);
    this.packages = this.packages.filter(function (el) {
      return el.id != s.id;
    });
    this.event.totalPrice = 0;
    this.event.oldtotalPrice = 0;
    this.packages.forEach(element => {
      this.event.totalPrice += parseInt(element.var_offer_price);
      this.event.oldtotalPrice += parseInt(element.var_price);
    });
    console.log(this.data);
  }

  submitPackage() {
    this.submitClicked = true;
    if (this.event.name.length > 0) {
      if (this.packages.length > 0) {
        console.log(this.packages);
        let payload;
        this.storage.get('loggedInEmployee').then((res) => {
          this.loggedInUser = res;
          let loading = this.loadingCtrl.create({
            content: 'Please wait...'
          });
          loading.present();
          if (this.event.isNew == true) {
            payload = {
              store_code: this.loggedInUser.currentStore.var_store_code,
              offer: {
                var_name: this.event.name,
                var_offer_price: this.event.totalPrice,
                var_price: this.event.oldtotalPrice,
                txt_note: this.event.note,
                dt_start_date: this.event.startDate,
                dt_end_date: this.event.endDate,
                is_new: this.event.isNew,
                enum_enable: (this.event.enum_enable ? 'YES' : 'NO'),
                services: this.packages
              }
            }
          } else {
            payload = {
              store_code: this.loggedInUser.currentStore.var_store_code,
              offer: {
                id: this.data.id,
                var_name: this.event.name,
                var_offer_price: this.event.totalPrice,
                var_price: this.event.oldtotalPrice,
                txt_note: this.event.note,
                dt_start_date: this.event.startDate,
                dt_end_date: this.event.endDate,
                is_new: this.event.isNew,
                enum_enable: (this.event.enum_enable ? 'YES' : 'NO'),
                services: this.packages
              }
            }
          }

          console.log('payload', payload);
          this.api.addStoreOffers(payload).subscribe(data => {
            console.log(JSON.parse(data._body));
            data = JSON.parse(data._body)
            if (data.status == 'error') {
              this.api.getToastMessage(data.message);
            } else {
              this.api.getToastMessage(`Offer "${this.event.name}" save succesfully`);
              this.viewCtrl.dismiss(this.data);
            }
            loading.dismiss();
          }, error => {
            loading.dismiss();
            this.api.showServerError();
            console.log(error);
          })
        })
      } else {
        let alert = this.alertCtrl.create({
          title: 'Add Service',
          message: 'Please add atleast one service',
          buttons: [
            {
              text: 'Ok',
              role: 'Ok',
              handler: () => {
                console.log('Ok clicked');
                // this.navCtrl.pop();
              }
            }
          ]
        });
        alert.present();
      }
    }
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  deleteOffer() {
    let alert = this.alertCtrl.create({
      title: 'Remove Offer',
      message: `Do you want to remove "${this.event.name}" offer?`,
      buttons: [
        {
          text: 'Oops, No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes, Remove',
          handler: () => {
            let loading = this.loadingCtrl.create({
              content: 'Please wait...'
            });
            loading.present();
            this.api.deleteOffer({ offer_id: this.data.id }).subscribe(data => {
              console.log(data);
              this.viewCtrl.dismiss();
              loading.dismiss();
              this.api.getToastMessage(`Offer "${this.event.name}" deleted successfully`);
            }, error => {
              loading.dismiss();
              this.api.showServerError();
              console.log(error);
            })
          }
        }
      ]
    });
    alert.present();
  }

}
