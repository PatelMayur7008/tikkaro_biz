import * as moment from 'moment';
import { Component, ViewChild, OnInit } from '@angular/core';
import { IonicPage, Nav, NavController, ModalController, LoadingController, ViewController, NavParams, ToastController } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { API } from '../../../services/api-service';

@Component({
  selector: 'page-add-offers',
  templateUrl: 'add-offers.html'
})
export class AddoffersPage implements OnInit {
  public event = {
    name: '',
    price: '',
    oldPrice: '',
    id: '',
    isNew: false,
    isAdd: false
  }
  loggedInUser: any;
  arrayOfStrings: any;
  selectedVenue: any;
  public submitClicked: boolean = false;
  already: any;
  arrayOfStringsAll: any = [];
  constructor(public navCtrl: NavController,
    public viewCtrl: ViewController,
    private navParams: NavParams,
    public modalCtrl: ModalController,
    private storage: Storage,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private api: API,
  ) {
    if (this.navParams.data.package) {
      console.log('data', this.navParams.data.package);
      this.event.name = this.navParams.data.package.var_title;
      this.event.id = this.navParams.data.package.id;
      this.event.price = this.navParams.data.package.var_price;
    } else {
      console.log('else data', this.navParams.data.package);

      if (this.navParams.data.already) {
        this.event.isAdd = true
        this.already = this.navParams.data.already;
      }
      console.log('datasdssssss', this.already);
    }

  }

  ngOnInit() {
    this.getAllServices();
  }

  getAllServices() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.storage.get('loggedInEmployee').then((res) => {
      this.loggedInUser = res;
      this.api.getStoreService({ store_code: this.loggedInUser.currentStore.var_store_code }).subscribe(data => {
        console.log(JSON.parse(data._body).data);
        // this.arrayOfStringsAll = ;
        this.arrayOfStringsAll = JSON.parse(data._body).data.filter(function (el) {
          return el.enum_enable == 'YES';
        });
        console.log(this.arrayOfStringsAll,'this.arrayOfStringsAll');
        this.arrayOfStrings = this.arrayOfStringsAll.map(a => a.var_title);
        console.log(this.arrayOfStrings,'this.arrayOfStrings');
        if (this.already) {
          this.already.forEach(element => {
            this.arrayOfStrings = this.arrayOfStrings.filter(function (el) {
              return el != element.var_title;
            });
          });
        }
        loading.dismiss();
      }, error => {
        loading.dismiss();
        this.api.showServerError();
        console.log(error);
      })
    })
  }

  addService() {
    // console.log(this.event.price,'this.event.price');
    // console.log(this.selectedVenue.var_price,'this.selectedVenue.var_price');
    this.submitClicked = true;
    var isValid = true;

    if(this.api.getRequiredValidation(this.selectedVenue.var_price)) {
        isValid = false;
    } else if (parseInt(this.event.price) >= parseInt(this.selectedVenue.var_price)) {
      let toast = this.toastCtrl.create({
        message: 'Please enter offer price less then service price',
        duration: 2000,
        position: 'bottom'
      });
      toast.present();
      isValid = false;
    }

    if (this.event.name != '' && this.event.price != '' && parseFloat(this.event.price) > 0 && isValid) {
      let index = this.arrayOfStrings.indexOf(this.event.name);
      this.arrayOfStringsAll.forEach(element => {
        if (element.var_title == this.event.name) {
          this.event.id = element.id;
          this.event.oldPrice = element.var_price;
        }
      });
      console.log(this.event);
      this.viewCtrl.dismiss({ data: this.event, other: this.navParams.data.package });
    }
  }

  getVenueName(venueName) {
    this.selectedVenue = this.arrayOfStringsAll.filter(a => a.var_title == venueName)[0];
    console.log(this.selectedVenue);

  }

  closeModal() {
    this.viewCtrl.dismiss();
  }


}
