import { OffersDetailPage } from './offers-detail/offers-detail';
import * as moment from 'moment';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, Nav, NavController, ModalController, LoadingController, ToastController,AlertController } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { API } from '../../services/api-service';

@Component({
  selector: 'page-offers',
  templateUrl: 'offers.html'
})

export class OffersPage {
  customerservice: any = [];
  loggedInUser: any;
  offers: any = [];

  constructor(public navCtrl: NavController,
    public modalCtrl: ModalController,
    private storage: Storage,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private api: API,
    private alertCtrl: AlertController
  ) {
    this.initializeItems();
  }

  goTopackagesDetail(c) {
    console.log(c,'Offer');
    let modal = this.modalCtrl.create(OffersDetailPage, { package: c });
    modal.present();
    modal.onDidDismiss(data => {
      console.log(data);
      this.initializeItems();
    });
  }

  initializeItems() {
    console.log('initializeItems');
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.storage.get('loggedInEmployee').then((res) => {
      this.loggedInUser = res;
      this.api.getStoreOffers({ store_code: this.loggedInUser.currentStore.var_store_code }).subscribe(data => {
        this.offers = JSON.parse(data._body).data.offers;
        console.log(this.offers);
        loading.dismiss();
      }, error => {
        loading.dismiss();
        this.api.showServerError();
        console.log(error);
      })
    })
  }

  addPackage() {
    let modal = this.modalCtrl.create(OffersDetailPage);
    modal.present();
    modal.onDidDismiss(data => {
      console.log(data);
      this.initializeItems();
    });
  }

  deleteOffer(c) {
    console.log(c);
    let alert = this.alertCtrl.create({
      title: 'Remove Offer',
      message: `Do you want to remove "${c.var_name}" offer?`,
      buttons: [
        {
          text: 'Opps, No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes, Remove',
          handler: () => {
            console.log(c);
            let loading = this.loadingCtrl.create({
              content: 'Please wait...'
            });
            loading.present();
            this.api.deleteOffer({ offer_id: c.id }).subscribe(data => {
              console.log(data);
              this.initializeItems();
              loading.dismiss();
              this.api.getToastMessage(`Offer "${c.var_name}" deleted successfully`);
            }, error => {
              loading.dismiss();
              this.api.showServerError();
              console.log(error);
            })
          }
        }
      ]
    });
    alert.present();
  }

}
