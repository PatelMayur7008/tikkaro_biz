import { Component } from '@angular/core';
import * as moment from 'moment';
import { IonicPage, ViewController, NavParams, Nav, NavController, ModalController, LoadingController, ToastController, AlertController } from 'ionic-angular';
import { API } from '../../../services/api-service';
import { AddpackagePage } from '../add-package/add-package';
import { Storage } from '@ionic/storage';
import { AddServicesPage } from '../../services/add-services/add-services';

@Component({
  selector: 'page-package-detail',
  templateUrl: 'package-detail.html'
})
export class PackageDetailPage {
  data;
  loggedInUser: any;
  storeServices: any = [];
  userDetails;
  packages;
  public event = {
    name: '',
    totalPrice: 0,
    note: '',
    isNew: false,
    enum_enable: true
  }
  public submitClicked: boolean = false;

  constructor(public navCtrl: NavController,
    public viewCtrl: ViewController,
    private navParams: NavParams,
    private toastCtrl: ToastController,
    private alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private storage: Storage,
    public loadingCtrl: LoadingController,
    private api: API,

  ) {

    this.storage.get('loggedInEmployee').then((res) => {
      this.loggedInUser = res;
      this.api.getStoreService({ store_code: this.loggedInUser.currentStore.var_store_code }).subscribe(data => {
        console.log(JSON.parse(data._body).data);
        this.storeServices = JSON.parse(data._body).data;
        console.log('this.storeServices',this.storeServices);
      });
    });

    if (this.navParams.get('package')) {
      this.data = this.navParams.get('package');
      this.event.name = this.data.var_name;
      this.event.note = this.data.txt_note;
      this.event.enum_enable = (this.data.enum_enable == 'YES' ? true : false);
      this.packages = this.data.services;
      if (this.packages.length > 0) {
        this.event.totalPrice = 0;
        this.packages.forEach(element => {
          this.event.totalPrice += parseInt(element.var_price);
        });
      }
    } else {
      this.packages = [];
      this.event.isNew = true;
    }
    console.log('this.data', this.data);
    console.log('this.event', this.event);

  }

  goToeditPackage(c) {
    let modal = this.modalCtrl.create(AddpackagePage, { package: c });
    modal.present();
    modal.onDidDismiss(data => {
      console.log(data);
      if (data.data) {
        if (data.data.isAdd == false) {
          this.packages.forEach(element => {
            if (data.data.id == element.id) {
              element.id = data.data.id;
              element.var_price = data.data.price;
              element.var_title = data.data.name;
            }
          });
        } else {
          this.packages.push({
            id: data.data.id,
            var_price: data.data.price,
            var_title: data.data.name
          })
        }
        if (this.packages.length > 0) {
          this.event.totalPrice = 0;
          this.packages.forEach(element => {
            this.event.totalPrice += parseInt(element.var_price);
          });
        }
      }
      console.log(this.data);
    });
  }

  goToaddPackage() {
    if (this.storeServices.length > 0) {
      let modal = this.modalCtrl.create(AddpackagePage, { already: this.packages });
      modal.present();
      modal.onDidDismiss(data => {
        console.log('ffff', data);
        if (data.data) {
          if (data.data.isAdd == false) {
            console.log('ifff')
            this.packages.forEach(element => {
              if (data.data.id == element.id) {
                element.id = data.data.id;
                element.var_price = data.data.price;
                element.var_title = data.data.name;
              }
            });
          } else {
            console.log('else')
            // if (data.data.isNew == true) {
            this.packages.push({
              id: data.data.id,
              var_price: data.data.price,
              var_title: data.data.name,
              is_new: true
            })
            // }else{
            // this.packages.push({
            //   id: data.data.id,
            //   var_price: data.data.price,
            //   var_title: data.data.name,
            //   is_new: false
            // })
            // }
          }
        }

        if (this.packages.length > 0) {
          this.event.totalPrice = 0;
          this.packages.forEach(element => {
            this.event.totalPrice += parseInt(element.var_price);
          });
        }

        console.log(this.packages)
      });
    } else { 
      let toast = this.toastCtrl.create({
        message: 'Please atleast one service add in your store.',
        duration: 1000,
        position: 'bottom'
      });
      toast.present();
      toast.onDidDismiss(() => {
        this.navCtrl.setRoot(AddServicesPage);
      });
    }
  }

  deleteService(s) {
    console.log(s);
    this.packages = this.packages.filter(function (el) {
      return el.id != s.id;
    });
    this.event.totalPrice = 0;
    this.packages.forEach(element => {
      this.event.totalPrice += parseInt(element.var_price);
    });
    console.log(this.data);
  }

  submitPackage() {
    this.submitClicked = true;

    if (this.event.name.length > 0) {
      if (this.packages.length > 0) {
        console.log(this.packages);
        let payload;
        this.storage.get('loggedInEmployee').then((res) => {
          this.loggedInUser = res;
          let loading = this.loadingCtrl.create({
            content: 'Please wait...'
          });
          loading.present();
          // this.packages.forEach(element => {
          //   if (element.is_new == true) {
          //     payload = {
          //       store_code: this.loggedInUser.currentStore.var_store_code,
          //       package: {
          //         var_name: this.event.name,
          //         var_price: this.event.totalPrice,
          //         txt_note: this.event.note,
          //         is_new: true,
          //         enum_enable: "YES",
          //         services: this.packages
          //       }
          //     }
          //   } else {
          //     payload = {
          //       store_code: this.loggedInUser.currentStore.var_store_code,
          //       package: {
          //         id: this.data.id,
          //         var_name: this.event.name,
          //         var_price: this.event.totalPrice,
          //         txt_note: this.event.note,
          //         is_new: false,
          //         enum_enable: "YES",
          //         services: this.packages
          //       }
          //     }
          //   }
          // });

          if (this.event.isNew == true) {
            payload = {
              store_code: this.loggedInUser.currentStore.var_store_code,
              package: {
                var_name: this.event.name,
                var_price: this.event.totalPrice,
                txt_note: this.event.note,
                is_new: this.event.isNew,
                enum_enable:(this.event.enum_enable ? 'YES' : 'NO'),
                services: this.packages
              }
            }
          } else {
            payload = {
              store_code: this.loggedInUser.currentStore.var_store_code,
              package: {
                id: this.data.id,
                var_name: this.event.name,
                var_price: this.event.totalPrice,
                txt_note: this.event.note,
                is_new: this.event.isNew,
                enum_enable: (this.event.enum_enable ? 'YES' : 'NO'),
                services: this.packages
              }
            }
          }
          console.log('payload', payload);
          this.api.addStorePackage(payload).subscribe(data => {
            console.log(JSON.parse(data._body));
            loading.dismiss();
            this.viewCtrl.dismiss(this.data);
            this.api.getToastMessage(`Package "${this.event.name}" saved successfully`);
          }, error => {
            loading.dismiss();
            this.api.showServerError();
            console.log(error);
          })
        })
      } else {
        let alert = this.alertCtrl.create({
          title: 'Add Service',
          message: 'Please add atleast one service',
          buttons: [
            {
              text: 'Ok',
              role: 'Ok',
              handler: () => {
                console.log('Ok clicked');
                // this.navCtrl.pop();
              }
            }
          ]
        });
        alert.present();
      }
    }
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  deletePackage() {
    let alert = this.alertCtrl.create({
      title: 'Remove Package',
      message: `Do you want to remove "${this.event.name}" package?`,
      buttons: [
        {
          text: 'Oops, No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes, Remove',
          handler: () => {
            let loading = this.loadingCtrl.create({
              content: 'Please wait...'
            });
            loading.present();
            this.api.deletePackage({ package_id: this.data.id }).subscribe(data => {
              console.log(data);
              loading.dismiss();
              this.viewCtrl.dismiss();
              this.api.getToastMessage(`Package "${this.event.name}" deleted successfully`);
            }, error => {
              loading.dismiss();
              this.api.showServerError();
              console.log(error);
            })
          }
        }
      ]
    });
    alert.present();
  }


}
