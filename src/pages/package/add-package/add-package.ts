import * as moment from 'moment';
import { Component, ViewChild, OnInit } from '@angular/core';
import { IonicPage, Nav, NavController, ModalController, LoadingController, ViewController, NavParams, ToastController } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { API } from '../../../services/api-service';

@Component({
  selector: 'page-add-package',
  templateUrl: 'add-package.html'
})
export class AddpackagePage implements OnInit {
  public event = {
    name: '',
    price: '',
    id: '',
    isNew: false,
    isAdd: false
  }
  loggedInUser: any;
  public submitClicked: boolean = false;
  arrayOfStrings: any;
  selectedVenue: any;
  already: any;
  arrayOfStringsAll: any = [];
  constructor(public navCtrl: NavController,
    public viewCtrl: ViewController,
    private navParams: NavParams,
    public modalCtrl: ModalController,
    private storage: Storage,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private api: API,
  ) {
    if (this.navParams.data.package) {
      //console.log('data', this.navParams.data.package);
      this.event.name = this.navParams.data.package.var_title;
      this.event.id = this.navParams.data.package.id;
      this.event.price = this.navParams.data.package.var_price;
    } else {
      //console.log('else data', this.navParams.data.package);

      if (this.navParams.data.already) {
        this.event.isAdd = true
        this.already = this.navParams.data.already;
      }
      //console.log('datasdssssss', this.already);
    }

  }

  ngOnInit() {
    this.getAllServices();
  }

  getAllServices() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.storage.get('loggedInEmployee').then((res) => {
      this.loggedInUser = res;
      this.api.getAllServciesList().subscribe(data => {
        //console.log(JSON.parse(data._body));
        this.arrayOfStringsAll = JSON.parse(data._body);
        this.arrayOfStrings = JSON.parse(data._body).map(a => a.var_title);
        //console.log(this.arrayOfStrings);
        if (this.already) {
          this.already.forEach(element => {
            this.arrayOfStrings = this.arrayOfStrings.filter(function (el) {
              return el != element.var_title;
            });
          });
        }
        loading.dismiss();
      }, error => {
        loading.dismiss();
        this.api.showServerError();
        //console.log(error);
      })
    })
  }

  addService() {
    this.submitClicked = true;
    if (this.event.name != '' && this.event.price != '' && parseFloat(this.event.price) > 0) {
      let index = this.arrayOfStrings.indexOf(this.event.name);
      this.arrayOfStringsAll.forEach(element => {
        if (element.var_title == this.event.name) {
          this.event.id = element.id;
        }
      });
      // if (index < 0) {
      //   this.event.isNew = true;
      // } else {
      //   this.event.isNew = false;
      // }
      //console.log(this.event);
      this.viewCtrl.dismiss({ data: this.event, other: this.navParams.data.package });
    }

  }

  getVenueName(venueName) {
    this.selectedVenue = this.arrayOfStringsAll.filter(a => a.var_title == venueName);
    //console.log(this.selectedVenue);

  }

  closeModal() {
    this.viewCtrl.dismiss();
  }


}
