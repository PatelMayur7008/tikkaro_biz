import * as moment from 'moment';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, Nav, NavController, ModalController, LoadingController, ToastController, AlertController } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { API } from '../../services/api-service';
import { PackageDetailPage } from './package-detail/package-detail';

@Component({
  selector: 'page-package',
  templateUrl: 'package.html'
})

export class PackagePage {
  customerservice: any = [];
  loggedInUser: any;
  packages: any = [];

  constructor(public navCtrl: NavController,
    public modalCtrl: ModalController,
    private storage: Storage,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private api: API,
    private alertCtrl: AlertController
  ) {
    this.initializeItems();
  }

  goTopackagesDetail(c) {
    let modal = this.modalCtrl.create(PackageDetailPage, { package: c });
    modal.present();
    modal.onDidDismiss(data => {
      //console.log(data);
      this.initializeItems();
    });
  }

  initializeItems() {
    //console.log('initializeItems');
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.storage.get('loggedInEmployee').then((res) => {
      this.loggedInUser = res;
      this.api.getStorePackage({ store_code: this.loggedInUser.currentStore.var_store_code }).subscribe(data => {
        this.packages = JSON.parse(data._body).data.packages;
       // console.log(this.packages);
        loading.dismiss();
      }, error => {
        loading.dismiss();
        this.api.showServerError();
        //console.log(error);
      })
    })
  }

  addPackage() {
    let modal = this.modalCtrl.create(PackageDetailPage);
    modal.present();
    modal.onDidDismiss(data => {
     // console.log(data);
      this.initializeItems();
    });
  }

  deletePackage(c) {
    console.log(c);
    let alert = this.alertCtrl.create({
      title: 'Remove Package',
      message: `Do you want to remove "${c.var_name}" package?`,
      buttons: [
        {
          text: 'Opps, No',
          role: 'cancel',
          handler: () => {
            //console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes, Delete',
          handler: () => {
            //console.log(c);
            let loading = this.loadingCtrl.create({
              content: 'Please wait...'
            });
            loading.present();
            this.api.deletePackage({ package_id: c.id }).subscribe(data => {
              //console.log(data);
              this.initializeItems();
              loading.dismiss();
              this.api.getToastMessage(`Package "${c.var_name}" deleted successfully`);
            }, error => {
              loading.dismiss();
              this.api.showServerError();
              //console.log(error);
            })
          }
        }
      ]
    });
    alert.present();
  }

}
