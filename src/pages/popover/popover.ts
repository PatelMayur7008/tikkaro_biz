import { Component } from '@angular/core';
import { NavController, LoadingController, PopoverController, ViewController } from 'ionic-angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
// import { FileChooser } from '@ionic-native/file-chooser';
import { DashboardPage } from '../dashboard/dashboard';
import { Storage } from '@ionic/storage';
import { ImagePicker } from '@ionic-native/image-picker';
import { API_BASE_URL } from '../../services/core/constants';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { utilService } from '../../services/core/util-service';

@Component({
  selector: 'page-popover',
  templateUrl: 'popover.html'
})
export class PopoverPage {
  fileTransfer: FileTransferObject;
  imageData;
  URI;
  private API_BASE_URL: any = API_BASE_URL;
  constructor(public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    private transfer: FileTransfer,
    public toastCtrl: ToastController,
    // private fileChooser: FileChooser,
    private utilService: utilService,
    private storage: Storage,
    public viewCtrl: ViewController,
    private imagePicker: ImagePicker,


  ) {
    this.fileTransfer = this.transfer.create();
  }

  uploadImageOLD() {
    let options;
    this.storage.get('loggedInEmployee').then((res) => {
      this.imagePicker.getPictures(options).then((results) => {
        let onearry = [];
        let loading = this.loadingCtrl.create({
          content: 'Please wait...'
        });
        loading.present();
        for (var i = 0; i < results.length; i++) {
          this.URI = results[i];
          let options1: FileUploadOptions = {
            fileKey: 'file',
            fileName: `${res.data.var_store_code}.jpg`,
            headers: {}
          }
          this.fileTransfer.upload(this.URI, API_BASE_URL + '/addStoreImage', options1)
            .then((data) => {
              alert("success" + JSON.stringify(data));
              onearry.push('success');
            }, (err) => {
              alert("error" + JSON.stringify(err));
            });
        }

        if (results.length == onearry.length) {
          loading.dismiss();
          this.viewCtrl.dismiss();
        }
      }, (err) => { });
    })

  }


  uploadImage() {
    let options;
    this.storage.get('loggedInEmployee').then((res) => {
      this.imagePicker.getPictures(options).then((results) => {
        let loader = this.loadingCtrl.create({
          content: "Uploading..."
        });
        loader.present();
        for (var i = 0; i < results.length; i++) {

          console.log(results);

          let loopContinue = true;
          let i = 0;
          const runConnectivity = async () => {

            while (loopContinue) {
              let options: FileUploadOptions = {
                fileKey: 'file',
                fileName: `${res.currentStore.var_store_code}.jpg`,
                httpMethod: 'POST',
                mimeType: 'image/jpg',
                chunkedMode: false,
                params: {
                  store_code: res.currentStore.var_store_code
                },
                headers: { "Accept": "image/jpg" }
              };
              if (results[i] !== undefined) {
                console.log(options);
                let data = await this.fileTransfer.upload(results[i], API_BASE_URL + '/addStoreImage', options);
                console.log(data);
                if (data === undefined || results.length < i - 1) {
                  loader.dismiss();
                  loopContinue = false;
                  this.presentToast("Image uploaded successfully");
                }
              } else {
                loader.dismiss();
                loopContinue = false;
                this.presentToast("Image uploaded successfully");
              }
              i++;
            }
          }
          runConnectivity();



          // var loopContinue = true;
          // const runConnectivity = async () => {
          //   while (loopContinue) {
          //     let connectivityResponse = await (
          //       this.fileTransfer.upload(results[i], API_BASE_URL + '/addStoreImage', options)
          //         .then((data) => {
          //           console.log(JSON.stringify(data));
          //           console.log(JSON.parse(JSON.stringify(data)));
          //           loopContinue = true;
          //         }, (err) => {
          //           console.log('error', err);
          //           loader.dismiss();
          //           loopContinue = false;
          //           this.presentToast(err);
          //         })
          //     );
          //     if (connectivityResponse === undefined) {
          //       loopContinue = false;
          //       loader.dismiss();
          //       this.presentToast("Image uploaded successfully");
          //     }
          //   }
          // }
          // runConnectivity();


        }
      }, (err) => { });
    })

  }


  uploadImageNew() {
    let ressss = '';
    let options;
    this.storage.get('loggedInEmployee').then((res) => {
      this.imagePicker.getPictures(options).then((results) => {
        let loader = this.loadingCtrl.create({
          content: "Uploading..."
        });
        loader.present();
        let loopContinue = true;
        let i = 0;
        const runConnectivity = async () => {
          while (loopContinue) {
            let options: FileUploadOptions = {
              fileKey: 'file',
              fileName: `${res.currentStore.var_store_code}.jpg`,
              httpMethod: 'POST',
              mimeType: 'image/jpg',
              chunkedMode: false,
              params: {
                store_code: res.currentStore.var_store_code
              },
              headers: { "Accept": "image/jpg",
              'Authorization': 'bearer ' + this.utilService.getToken(),
            }
            };
            if (results[i] !== undefined) {
              console.log(results[i]);
              let data = await this.fileTransfer.upload(results[i], API_BASE_URL + '/addStoreImage', options);
              console.log(data);
              console.log(JSON.parse(JSON.stringify(data)));
              let res = JSON.parse(JSON.parse(JSON.stringify(data)).response);
              ressss = res.message;
              console.log(ressss);
              if (data === undefined || results.length < i - 1) {
                loader.dismiss();
                loopContinue = false;
                console.log('toast------', ressss);
                this.presentToast(ressss);
                // this.presentToast("Image uploaded successfully");
              }
            } else {
              loader.dismiss();
              loopContinue = false;
              console.log('toast------', ressss);
              this.presentToast(ressss);
              // this.presentToast("Image uploaded successfully");
            }
            i++;
          }
        }
        runConnectivity();
      }, (err) => { });
    })

  }


  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    toast.present();
    this.viewCtrl.dismiss();
  }

}
