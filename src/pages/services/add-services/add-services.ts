import * as moment from 'moment';
import { Component, ViewChild, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { IonicPage, Nav, NavController, ModalController, LoadingController, ViewController, NavParams, ToastController, AlertController, ActionSheetController } from 'ionic-angular';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { Storage } from '@ionic/storage';
import { API } from '../../../services/api-service';

@Component({
  selector: 'page-add-services',
  templateUrl: 'add-services.html'
})
export class AddServicesPage implements OnInit {
  myControl: FormControl = new FormControl();
  hours = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
  minutes = ['00', '15', '30', '45'];
  public event = {
    name: '',
    timeHr: '',
    timeMin: '',
    price: '',
    id: '',
    isNew: false,
    isAdd: false,
    enum_enable: true
  }
  public submitClicked: boolean = false;

  loggedInUser: any;
  arrayOfStrings: any;
  restServices: any = [];
  selectedVenue: any;
  arrayOfStringsAll: any = [];
  constructor(public navCtrl: NavController,
    public viewCtrl: ViewController,
    private alertCtrl: AlertController,
    private navParams: NavParams,
    public modalCtrl: ModalController,
    private storage: Storage,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private api: API,
    public actionSheetCtrl: ActionSheetController,
  ) {
    console.log(this.navParams.data.storeServices, 'this.navParams.data');

    // var storeServices = this.navParams.data.storeServices;
    if (this.navParams.data.staff) {
      console.log('data', this.navParams.data.staff);
      this.event.name = this.navParams.data.staff.var_title;
      this.event.id = this.navParams.data.staff.id;
      this.event.price = this.navParams.data.staff.var_price;
      this.event.timeHr = this.navParams.data.staff.dt_service_time.split(":")[0];
      this.event.timeMin = this.navParams.data.staff.dt_service_time.split(":")[1];
      this.event.enum_enable = (this.navParams.data.staff.enum_enable == 'YES' ? true : false);
    } else {
      console.log('datasdssssss');
      this.event.isAdd = true
    }

  }

  ngOnInit() {
    this.getAllServices();
  }

  getAllServices() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.storage.get('loggedInEmployee').then((res) => {
      this.loggedInUser = res;
      this.api.getAllServciesList().subscribe(data => {
        console.log(JSON.parse(data._body));
        this.arrayOfStringsAll = JSON.parse(data._body);

        console.log(this.navParams.data.storeServices, 'Nav PARAMS SERVICES');
        console.log(this.arrayOfStringsAll, 'arrayOfStringsAll');
        if (typeof this.navParams.data.storeServices !== 'undefined') {
          this.restServices = this.arrayOfStringsAll.filter(o => !this.navParams.data.storeServices.find(o2 => o.id === o2.id))
        }else{
          this.restServices = this.arrayOfStringsAll;
        }
        console.log(this.arrayOfStringsAll, 'arrayOfStringsAll 123');


        // var stroeHasService =  this.navParams.data.storeServices;
        // var result = this.arrayOfStringsAll.filter(function (o1) {
        //   return stroeHasService.some(function (o2) {
        //     console.log('O1 ID' , o1.id, 'O2 ID',o2.id)
        //     return o1.id !== o2.id; // return the ones with equal id
        //   });
        // });

        // console.log(result,'result');

        this.restServices = this.restServices.map(a => a.var_title);

        this.arrayOfStrings = this.arrayOfStringsAll.map(a => a.var_title);
        console.log(this.arrayOfStrings);

        loading.dismiss();
      }, error => {
        loading.dismiss();
        this.api.showServerError();
        console.log(error);
      })
    })
  }

  addService() {
    this.submitClicked = true;
    console.log(this.event);
    if (this.event.name != '' && parseFloat(this.event.price) > 0) {
      if ((this.event.timeHr == '00' && this.event.timeMin == "00") || (this.event.timeHr == '' && this.event.timeMin == '')) {
        this.api.getToastMessage('Service time must be minimum 15 min');
        return false;
      }

      let index = this.arrayOfStrings.indexOf(this.event.name);
      this.arrayOfStringsAll.forEach(element => {
        if (element.var_title == this.event.name) {
          this.event.id = element.id;
        }
      });
      // alert(index); return false;
      if (index < 0) {
        this.event.isNew = true;
        // var message = 'Service Added Successfully';
      } else {
        this.event.isNew = false;
        // var message = 'Service Updated Successfully';
      }
      // let toast = this.toastCtrl.create({
      //   message: message,
      //   duration: 1000,
      //   position: 'bottom'
      // });
      // toast.present();
      // toast.onDidDismiss(() => {
      //   console.log(this.event, 'this.event');
        this.viewCtrl.dismiss({ data: this.event, other: this.navParams.data.staff });
      // });
    }
  }

  getVenueName(venueName) {
    this.selectedVenue = this.arrayOfStringsAll.filter(a => a.var_title == venueName);
    console.log(this.selectedVenue);

  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  deleteServices(service) {
    console.log(service);
    let alert = this.alertCtrl.create({
      title: 'Delete Service',
      message: `Do you want to delete "${service.name}"?`,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Delete',
          handler: () => {
            let loading = this.loadingCtrl.create({
              content: 'Please wait...'
            });
            loading.present();
            this.api.deleteServices({ store_code: this.loggedInUser.currentStore.var_store_code, service_id: service.id }).subscribe(data => {
              this.api.getToastMessage(`Service "${service.name}" deleted successfully`);
              loading.dismiss();
              this.closeModal();
            }, error => {
              loading.dismiss();
              this.api.showServerError();
              console.log(error);
            })
          }
        }
      ]
    });
    alert.present();
  }

  selectServiceHr() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Hour',
      buttons: [
        {
          text: '00',
          handler: () => {
            this.event.timeHr = '00'
          }
        },
        {
          text: '01',
          handler: () => {
            this.event.timeHr = '01'
          }
        },
        {
          text: '02',
          handler: () => {
            this.event.timeHr = '02'
          }
        },
        {
          text: '03',
          handler: () => {
            this.event.timeHr = '03'
          }
        },
        {
          text: '04',
          handler: () => {
            this.event.timeHr = '04'
          }
        },
        {
          text: '05',
          handler: () => {
            this.event.timeHr = '04'
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            //console.log('Cancel clicked');
          }
        }
      ]
    });
 
    actionSheet.present();
  }

  selectServiceMin() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Minute',
      buttons: [
        {
          text: '00',
          handler: () => {
            this.event.timeMin = '00'
          }
        },
        {
          text: '15',
          handler: () => {
            this.event.timeMin = '15'
          }
        },
        {
          text: '30',
          handler: () => {
            this.event.timeMin = '30'
          }
        },
        {
          text: '45',
          handler: () => {
            this.event.timeMin = '45'
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            //console.log('Cancel clicked');
          }
        }
      ]
    });
 
    actionSheet.present();
  }

}
