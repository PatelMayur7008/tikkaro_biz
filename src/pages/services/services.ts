import * as moment from 'moment';
import { Component, ViewChild } from '@angular/core';
import { AddHolidayPage } from '../add-holiday/add-holiday';
import { IonicPage, Nav, NavController, ModalController, LoadingController, ToastController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { API } from '../../services/api-service';
import { DashboardPage } from '../dashboard/dashboard';
import { AddStaffPage } from '../staff/add-staff/add-staff';
import { AddServicesPage } from './add-services/add-services';


@Component({
  selector: 'page-services',
  templateUrl: 'services.html'
})
export class ServicesPage {
  staff: any = [];
  loggedInUser: any;
  staff1: any;

  constructor(public navCtrl: NavController,
    public modalCtrl: ModalController,
    private storage: Storage,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private api: API,
    private alertCtrl: AlertController

  ) {
    this.initializeItems();
  }

  selectService(service) {
    let modal = this.modalCtrl.create(AddServicesPage, { staff: service, storeServices: this.staff1 });
    modal.present();
    modal.onDidDismiss(data => {
      console.log('data', data);
      if (data) {
        let loading = this.loadingCtrl.create({
          content: 'Please wait...'
        });
        let payload = {
          store_code: this.loggedInUser.currentStore.var_store_code,
          service: {
            id: data.data.id,
            var_title: data.data.name,
            var_price: data.data.price,
            dt_service_time: `${(data.data.timeHr ? data.data.timeHr : '00')}:${(data.data.timeMin ? data.data.timeMin : '00')}:00`,
            is_new: data.data.isNew,
            enum_enable: (data.data.enum_enable == false ? 'NO' : 'YES')
          }
        }
        console.log(payload);
        loading.present();
        this.api.updateServices(payload).subscribe(data => {
          this.api.getToastMessage(`Service "${service.var_title}" Updated Successfully`);
          this.initializeItems();
          loading.dismiss();
        }, error => {
          loading.dismiss();
          this.api.showServerError();
          console.log(error);
        })
      } else {
        this.initializeItems();
      }
    });
  }

  deleteServices(user) {
    console.log(this.staff, 'this.staff');
    if (this.staff.length > 1) {
      let alert = this.alertCtrl.create({
        title: 'Delete Service',
        message: `Do you want to delete "${user.var_title}"?`,
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Delete',
            handler: () => {
              console.log('user', user);
              let loading = this.loadingCtrl.create({
                content: 'Please wait...'
              });
              loading.present();
              this.api.deleteServices({ store_code: this.loggedInUser.currentStore.var_store_code, service_id: user.id }).subscribe(data => {
                this.api.getToastMessage(`Service "${user.var_title}" deleted successfully`);
                loading.dismiss();
                this.initializeItems();
              }, error => {
                loading.dismiss();
                this.api.showServerError();
                console.log(error);
              })
            }
          }
        ]
      });
      alert.present();
    } else {
      let toast = this.toastCtrl.create({
        message: "You can't delete all services",
        duration: 2000,
        position: 'bottom'
      });
      toast.present();
      toast.onDidDismiss(() => {
        // this.navCtrl.setRoot(DashboardPage);
      });
    }

  }

  initializeItems() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.storage.get('loggedInEmployee').then((res) => {
      this.loggedInUser = res;
      this.api.getServciesList({ store_code: this.loggedInUser.currentStore.var_store_code }).subscribe(data => {
        this.staff1 = JSON.parse(data._body).data;
        if (typeof this.staff1 !== 'undefined') {
          this.staff1.forEach(t => {
            t.dt_service_time = moment(t.dt_service_time, "HH:mm:ss").format("HH:mm")
          });
        }
        loading.dismiss();
        this.filterItem();
      }, error => {
        loading.dismiss();
        this.api.showServerError();
        console.log(error);
      })
    })
  }

  filterItem() {
    this.staff = this.staff1;
    console.log(this.staff);
  }

  getItems(ev) {
    this.filterItem();
    var val = ev.target.value;
    if (val && val.trim() != '') {
      this.staff = this.staff.filter((item) => {
        return ((item.var_title.toLowerCase().indexOf(val.toLowerCase()) > -1) ||
          (item.var_price.toLowerCase().indexOf(val.toLowerCase()) > -1));
      })
    }
  }

  addServices() {
    let modal = this.modalCtrl.create(AddServicesPage, { staff: '', storeServices: this.staff1 });
    modal.present();
    modal.onDidDismiss(data => {
      console.log(data);
      if (data) {
        let payload = {
          store_code: this.loggedInUser.currentStore.var_store_code,

          service: {
            id: data.data.id,
            var_title: data.data.name,
            var_price: data.data.price,
            dt_service_time: `${(data.data.timeHr ? data.data.timeHr : '00')}:${(data.data.timeMin ? data.data.timeMin : '00')}:00`,
            is_new: data.data.isNew,
            enum_enable: (data.data.enum_enable == false ? 'NO' : 'YES')
          }
        }
        console.log(payload);
        let loading = this.loadingCtrl.create({
          content: 'Please wait...'
        });
        loading.present();
        this.api.updateServices(payload).subscribe(data => {
          this.api.getToastMessage(`Service "${payload.service.var_title}" added successfully`);
          loading.dismiss();
          this.initializeItems();
        }, error => {
          loading.dismiss();
          this.api.showServerError();
          console.log(error);
        })
      }
    });
  }
}
