import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import * as moment from 'moment';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  currentDate: any;
  nextDate: any;
  prevDate: any;
  tabbing: any;
  completedservice = [
    {
      total: 500,
      service: ['hair', 'saving'],
      timefrom: '2:00',
      timeto: '3:00',
    },
    {
      total: 200,
      service: ['hair', 'savin111g'],
      timefrom: '7:00',
      timeto: '8:00',
    },
    {
      total: 100,
      service: ['hair111', 'saving'],
      timefrom: '6:00',
      timeto: '7:00',
    },
    {
      total: 50,
      service: ['hair', 'savi222ng'],
      timefrom: '5:00',
      timeto: '6:00',
    },
    {
      total: 250,
      service: ['hai222', 'saving'],
      timefrom: '1:00',
      timeto: '2:00',
    },
    {
      total: 300,
      service: ['hair', 'sav333ing'],
      timefrom: '1:00',
      timeto: '4:00',
    }];
  upcomingservice = [
    {
      total: 20,
      service: ['hair', 'saving'],
      timefrom: '2:00',
      timeto: '3:00',
    },
    {
      total: 2200,
      service: ['hair', 'savin111g'],
      timefrom: '7:00',
      timeto: '8:00',
    },
    {
      total: 1100,
      service: ['hair111', 'saving'],
      timefrom: '6:00',
      timeto: '7:00',
    },
    {
      total: 510,
      service: ['hair', 'savi222ng'],
      timefrom: '5:00',
      timeto: '6:00',
    },
    {
      total: 2510,
      service: ['hai222', 'saving'],
      timefrom: '1:00',
      timeto: '2:00',
    },
    {
      total: 3001,
      service: ['hair', 'sav333ing'],
      timefrom: '1:00',
      timeto: '4:00',
    }];
  cancelledservice = [
    {
      total: 5002,
      service: ['hair', 'saving'],
      timefrom: '2:00',
      timeto: '3:00',
    },
    {
      total: 2002,
      service: ['hair', 'savin111g'],
      timefrom: '7:00',
      timeto: '8:00',
    },
    {
      total: 1001,
      service: ['hair111', 'saving'],
      timefrom: '6:00',
      timeto: '7:00',
    },
    {
      total: 150,
      service: ['hair', 'savi222ng'],
      timefrom: '5:00',
      timeto: '6:00',
    },
    {
      total: 2150,
      service: ['hai222', 'saving'],
      timefrom: '1:00',
      timeto: '2:00',
    },
    {
      total: 3100,
      service: ['hair', 'sav333ing'],
      timefrom: '1:00',
      timeto: '4:00',
    }];
  constructor(public navCtrl: NavController) {
    console.log('home');

    this.tabbing = "completed";
    this.currentDate = moment().format('DD MMMM YYYY');
    this.nextDate = moment(this.currentDate, "DD MMMM YYYY").add('days', 1).format('DD MMMM YYYY');
    this.prevDate = moment(this.currentDate, "DD MMMM YYYY").add('days', -1).format('DD MMMM YYYY');
  }

  nextDay(selectedDate) {
    this.nextDate = moment(selectedDate, "DD MMMM YYYY").add('days', 1).format('DD MMMM YYYY');
    this.prevDate = moment(selectedDate, "DD MMMM YYYY").add('days', -1).format('DD MMMM YYYY');
    this.currentDate = moment(selectedDate).format('DD MMMM YYYY');
  }

  prevDay(selectedDate) {
    this.nextDate = moment(selectedDate, "DD MMMM YYYY").add('days', 1).format('DD MMMM YYYY');
    this.prevDate = moment(selectedDate, "DD MMMM YYYY").add('days', -1).format('DD MMMM YYYY');
    this.currentDate = moment(selectedDate).format('DD MMMM YYYY');
  }

  tabchange(currentTab) {
    console.log('currentTab', currentTab);
  }

  selectUser() {

  }

}
