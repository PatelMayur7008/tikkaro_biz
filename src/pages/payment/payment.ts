import { Component } from '@angular/core';
import { NavController, ViewController, NavParams, ToastController, LoadingController, AlertController } from 'ionic-angular';
import * as moment from 'moment';
import { Storage } from '@ionic/storage';
import { API } from '../../services/api-service';
import { utilService } from '../../services/core/util-service';

@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html'
})
export class PaymentPage {
  public event = {
    amount: 0,
    note: '',
  }
  loggedInUser: any;
  totalAmount;
  totalGetamount;
  activeTab: string = 'Paid';
  public submitClicked: boolean = false;

  paymentData: any;
  constructor(public navCtrl: NavController,
    public viewCtrl: ViewController,
    private storage: Storage,
    private navParams: NavParams,
    private alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private api: API,
    private util: utilService,
  ) {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.storage.get('loggedInEmployee').then((res) => {
      this.loggedInUser = res;
      this.api.getPaymentHistory({ storeId: this.loggedInUser.currentStore.id }).subscribe(data => {
        this.paymentData = JSON.parse(data._body);
        this.totalAmount = this.paymentData.totalAmnt;
        this.totalGetamount = this.paymentData.receivableAmnt;
        console.log(this.paymentData);
        console.log(this.totalGetamount,'totalGetamount');
        loading.dismiss();
      }, error => {
        loading.dismiss();
        this.api.showServerError();
        console.log(error);
      })
    })
  }

  changeTab(currentTab) {
    console.log(currentTab)
  }

  showDoPayment() {
    let alert = this.alertCtrl.create({
      title: 'Request Payment',
      inputs: [
        {
          name: 'payment',
          placeholder: 'Enter Amount'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Request Payment',
          handler: data => {
            if (data.payment > 0) {
              this.event.amount = data.payment;
              this.dopayment();
              console.log(data.payment)
            } else {
              this.api.getToastMessage('Please enter amount greater than 0')
              return false;
            }
          }
        }
      ]
    });
    alert.present();
  }

  dopayment() {
    console.log('asd');
    console.log(this.event.amount);
    this.submitClicked = true;
    if (this.event.amount > 0) {
      if (this.event.amount <= this.totalGetamount) {
        let loading = this.loadingCtrl.create({
          content: 'Please wait...'
        });
        loading.present();
        this.api.requestPayment({ storeId: this.loggedInUser.currentStore.id, requestedAmnt: this.event.amount }).subscribe(data => {
          let response = JSON.parse(data._body);
          console.log(response);
          if (response.status != 'error') {
            this.navCtrl.setRoot(PaymentPage);
          }
          let toast = this.toastCtrl.create({
            message: response.message,
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
          toast.onDidDismiss(() => {
          });
          loading.dismiss();
        }, error => {
          loading.dismiss();
          this.api.showServerError();
          console.log(error);
        })
      } else {
        this.api.getToastMessage(`You can not ask more than ${this.totalGetamount}`)
      }
    }
  }

}
