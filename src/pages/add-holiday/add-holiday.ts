import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';
import * as moment from 'moment';

@Component({
  selector: 'page-add-holiday',
  templateUrl: 'add-holiday.html'
})
export class AddHolidayPage {
  public event = {
    date: moment().format('YYYY-MM-DD'),
    timeStarts: moment().format('HH:mm'),
    timeEnds: moment().format('HH:mm'),
    text: ''
  }
  constructor(public navCtrl: NavController,
    public viewCtrl: ViewController,
  ) {
  }

  addHoliday() {
    this.viewCtrl.dismiss(this.event);
  }
}
