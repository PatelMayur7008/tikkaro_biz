import { CustomerDetailPage } from './customer-detail/customer-detail';
import * as moment from 'moment';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, Nav, NavController, ModalController, LoadingController, ToastController, AlertController } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';
import { SocialSharing } from '@ionic-native/social-sharing';
import { EmailComposer } from '@ionic-native/email-composer';

import { Storage } from '@ionic/storage';
import { API } from '../../services/api-service';

@Component({
  selector: 'page-customer',
  templateUrl: 'customer.html'
})

export class CustomerPage {
  customerservice: any = [];
  staff: any = [];
  loggedInUser: any;
  staff1: any;

  constructor(public navCtrl: NavController,
    public modalCtrl: ModalController,
    private storage: Storage,
    private toastCtrl: ToastController,
    private emailComposer: EmailComposer,
    public loadingCtrl: LoadingController,
    private socialSharing: SocialSharing,
    private api: API, private callNumber: CallNumber,
    private alertCtrl: AlertController,
  ) {
    this.initializeItems();
  }

  goToCustomerDetail(c) {
    let modal = this.modalCtrl.create(CustomerDetailPage, { customer: c });
    modal.present();
    modal.onDidDismiss(data => {
      console.log(data);
    });
  }

  initializeItems() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.storage.get('loggedInEmployee').then((res) => {
      this.loggedInUser = res;
      this.api.getCustomerList({ store_code: this.loggedInUser.currentStore.var_store_code }).subscribe(data => {
        this.staff1 = JSON.parse(data._body);
        this.staff1.forEach(element => {
          setTimeout(() => {
            element.var_profile_image_thumb = element.var_profile_image;
          }, 500);
        });
        loading.dismiss();
        this.filterItem();
      }, error => {
        loading.dismiss();
        this.api.showServerError();
        console.log(error);
      })
    })
  }

  filterItem() {
    this.staff = this.staff1;
  }

  getItems(ev) {
    this.filterItem();
    var val = ev.target.value;
    console.log(val)
    console.log(this.staff)
    if (val && val.trim() != '') {
      this.staff = this.staff.filter((item) => {
        return ((item.username.toLowerCase().indexOf(val.toLowerCase()) > -1) ||
          (item.bint_phone.toLowerCase().indexOf(val.toLowerCase()) > -1));
      })
    }
  }

  call(e, c) {
    e.stopPropagation();
    let alert = this.alertCtrl.create({
      title: 'Call to customer',
      message: 'Do you want to call to customer?',
      buttons: [
        {
          text: 'Oops, No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes, Continue',
          handler: () => {
            let loading = this.loadingCtrl.create({
              content: 'Please wait...'
            });
            this.callNumber.callNumber(c.bint_phone, true)
            .then(() => console.log('Launched dialer!'))
            .catch(() => console.log('Error launching dialer'));
          }
        }
      ]
    });
    alert.present();
  }

  share(e, c) {
    e.stopPropagation();
    console.log(c)
    this.socialSharing.shareViaWhatsApp(`${c.username}`, c.var_profile_image).then(() => {
    }).catch(() => {
      alert("Please install Whatsapp");
    });
  }

  sendEmail(e, c) {
    e.stopPropagation();
    console.log(c)
    this.emailComposer.isAvailable().then((available: boolean) => {
      console.log(available)
      // if (available) {
        let email = {
          to: c.var_email,
          // cc: '',
          // bcc: ['', ''],
          // attachments: [
          //   'file://img/logo.png',
          //   'res://icon.png',
          //   'base64:icon.png//iVBORw0KGgoAAAANSUhEUg...',
          //   'file://README.pdf'
          // ],
          // subject: 'Cordova Icons',
          // body: 'How are you? Nice greetings from Leipzig',
          isHtml: true
        };
        this.emailComposer.open(email);
      // }
    });
  }

}
