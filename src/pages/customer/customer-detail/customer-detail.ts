import { BookingViewPage } from './../../booking-view/booking-view';
import { Component } from '@angular/core';
import * as moment from 'moment';
import { IonicPage, ViewController, NavParams, Nav, NavController, ModalController, LoadingController, ToastController, AlertController } from 'ionic-angular';
import { API } from '../../../services/api-service';
import { SocialSharing } from '@ionic-native/social-sharing';
import { CallNumber } from '@ionic-native/call-number';
import { Storage } from '@ionic/storage';
import { EmailComposer } from '@ionic-native/email-composer';

@Component({
  selector: 'page-customer-detail',
  templateUrl: 'customer-detail.html'
})
export class CustomerDetailPage {
  data;
  loggedInUser: any;
  userDetails;
  constructor(public navCtrl: NavController,
    public viewCtrl: ViewController,
    private navParams: NavParams,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private emailComposer: EmailComposer,
    private socialSharing: SocialSharing,
    private api: API,
    private storage: Storage,
    private callNumber: CallNumber,
    private alertCtrl: AlertController
  ) {
    this.data = this.navParams.get('customer');
    console.log('this.data', this.data);
    this.storage.get('loggedInEmployee').then((res) => {
      this.loggedInUser = res;
      this.getData();

    })
  }

  getData() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait 123...'
    });
    loading.present();
    this.api.getCustomerDetailsList({ user_id: this.data.id, store_code: this.loggedInUser.currentStore.var_store_code }).subscribe(data => {
      this.userDetails = JSON.parse(data._body);
      this.userDetails.forEach(element => {
        element.dt_booking_date_month = moment(element.dt_booking_date).format('MMM')
        element.dt_booking_date_day = moment(element.dt_booking_date).format('DD')
        element.timeslotFirst = moment(element.timeslot.split('-')[0], "HH:mm:ss").format("hh:mm A")
        element.timeslotSec = moment(element.timeslot.split('-')[1], "HH:mm:ss").format("hh:mm A")
      });
      console.log('this.userDetails', this.userDetails);
      loading.dismiss();
    }, error => {
      loading.dismiss();
      this.api.showServerError();
      console.log(error);
    })
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  share(e, data) {
    e.stopPropagation();
    console.log(data);
    this.socialSharing.shareViaWhatsApp(data.username, data.var_profile_image).then(() => {
    }).catch(() => {
      alert("Please install Whatsapp");
    });

  }

  call(e, data) {
    e.stopPropagation();
    let alert = this.alertCtrl.create({
      title: 'Call to customer',
      message: 'Do you want to call to customer?',
      buttons: [
        {
          text: 'Oops, No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes, Continue',
          handler: () => {
            let loading = this.loadingCtrl.create({
              content: 'Please wait...'
            });
            this.callNumber.callNumber(data.bint_phone, true)
              .then(() => console.log('Launched dialer!'))
              .catch(() => console.log('Error launching dialer'));
          }
        }
      ]
    });
    alert.present();
  }

  sendEmail(e, data) {
    e.stopPropagation();
    console.log(data)
    this.emailComposer.isAvailable().then((available: boolean) => {
      // if (available) {
      let email = {
        to: data.var_email,
        // cc: '',
        // bcc: ['', ''],
        // attachments: [
        //   'file://img/logo.png',
        //   'res://icon.png',
        //   'base64:icon.png//iVBORw0KGgoAAAANSUhEUg...',
        //   'file://README.pdf'
        // ],
        // subject: 'Cordova Icons',
        // body: 'How are you? Nice greetings from Leipzig',
        isHtml: true
      };
      this.emailComposer.open(email);
      // }
    });
  }

  goToBookingViewPage(booking) {
    // this.navCtrl.push(BookingViewPage);
    console.log(booking);
    this.navCtrl.push(BookingViewPage, { booking: booking, navigate: 'client' });

  }

}
