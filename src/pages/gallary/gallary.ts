import { Component } from '@angular/core';
import { NavController, LoadingController, PopoverController, AlertController } from 'ionic-angular';
// import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
// import { FileChooser } from '@ionic-native/file-chooser';
import { DashboardPage } from '../dashboard/dashboard';
import { PopoverPage } from '../popover/popover';
import { Storage } from '@ionic/storage';
import { API } from '../../services/api-service';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { FileTransfer, FileTransferObject, FileUploadOptions } from '@ionic-native/file-transfer';
import { ImagePicker } from '@ionic-native/image-picker';
import { utilService } from '../../services/core/util-service';
import { API_BASE_URL } from '../../services/core/constants';

@Component({
  selector: 'page-gallary',
  templateUrl: 'gallary.html'
})
export class GallaryPage {
  date: string;
  type: 'string';
  fileTransfer: FileTransferObject;
  imageData;
  loggedInUser: any;
  images: any = [];

  constructor(public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    private transfer: FileTransfer,
    // private fileChooser: FileChooser,
    private imagePicker: ImagePicker,
    public toastCtrl: ToastController,
    private popoverCtrl: PopoverController,
    private utilService: utilService,
    private alertCtrl: AlertController,
    private storage: Storage,
    private api: API,

  ) {
    this.fileTransfer = this.transfer.create();
    this.initializeItems();
  }
  onChange(e) {
  }

  choose() {
    let options = {
      quality: 100
    };
    // this.fileChooser.open()
    //   .then((imageData) => {
    //     this.imageData = imageData;

    // let options1: FileUploadOptions = {
    //   fileKey: 'file',
    //   fileName: `${imageData}.jpg`,
    //   headers: {}
    // }
    // this.fileTransfer.upload(imageData, 'http://8823f825.ngrok.io/my_ci/test/addImage', options1)
    //   .then((data) => {
    //     // success
    //     alert("success");
    //   }, (err) => {
    //     // error
    //     alert("error" + JSON.stringify(err));
    //   });
    // })
    // .catch(e => //console.log(e));

  }

  upload() {

  }

  initializeItems() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.storage.get('loggedInEmployee').then((res) => {
      this.loggedInUser = res;
      this.api.getImages({ store_code: this.loggedInUser.currentStore.var_store_code }).subscribe(data => {
        this.images = JSON.parse(data._body).data;
        //console.log(this.images);
        this.images.forEach(element => {
          setTimeout(() => {
            element.var_file_thumb = element.var_file;
          }, 500);
          element.enum_enable = (element.enum_enable == 'YES' ? true : false);
        });
        loading.dismiss();
      }, error => {
        loading.dismiss();
        this.api.showServerError();
        //console.log(error);
      })
    })
  }

  // presentPopover(ev) {
  //   // this.navCtrl.setPages(PopoverPage);
  //   // let popover = this.popoverCtrl.create(PopoverPage, {
  //   // });
  //   // popover.present({
  //   //   ev: ev
  //   // });
  //   // popover.onDidDismiss(response => {
  //   //   this.initializeItems();
  //   // });
  // }

  uploadImageNew() {
    let ressss = '';
    let options;
    this.storage.get('loggedInEmployee').then((res) => {
      this.imagePicker.getPictures(options).then((results) => {
        let loader = this.loadingCtrl.create({
          content: "Uploading..."
        });
        loader.present();
        let loopContinue = true;
        let i = 0;
        const runConnectivity = async () => {
          while (loopContinue) {
            let options: FileUploadOptions = {
              fileKey: 'file',
              fileName: `${res.currentStore.var_store_code}.jpg`,
              httpMethod: 'POST',
              mimeType: 'image/jpg',
              chunkedMode: false,
              params: {
                store_code: res.currentStore.var_store_code
              },
              headers: { "Accept": "image/jpg",
              'Authorization': 'bearer ' + this.utilService.getToken(),
            }
            };
            if (results[i] !== undefined) {
              //console.log(results[i]);
              let data = await this.fileTransfer.upload(results[i], API_BASE_URL + '/addStoreImage', options);
              //console.log(data);
              //console.log(JSON.parse(JSON.stringify(data)));
              let res = JSON.parse(JSON.parse(JSON.stringify(data)).response);
              ressss = res.message;
              //console.log(ressss);
              if (data === undefined || results.length < i - 1) {
                loader.dismiss();
                loopContinue = false;
                //console.log('toast------', ressss);
                this.presentToast(ressss);
                // this.presentToast("Image uploaded successfully");
              }
            } else {
              loader.dismiss();
              loopContinue = false;
              //console.log('toast------', ressss);
              this.presentToast(ressss);
              // this.presentToast("Image uploaded successfully");
            }
            i++;
          }
        }
        runConnectivity();
      }, (err) => { });
    })

  }

  
  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      //console.log('Dismissed toast');
    });
    toast.present();
    this.initializeItems();
  }

  setProfile(image) {
    //console.log(image);
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    //loading.present();
    this.api.setProfile({ store_code: this.loggedInUser.currentStore.var_store_code, img_id: image.id }).subscribe(data => {
      //console.log(JSON.parse(data._body));
      this.initializeItems();
      //loading.dismiss();
      this.api.getToastMessage('Set as profile status updated successfully')
    }, error => {
      //loading.dismiss();
      this.api.showServerError();
      //console.log(error);
    })
  }

  activeImg(image) {
    //console.log(image);
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    //loading.present();
    this.api.activeImg({ store_code: this.loggedInUser.currentStore.var_store_code, img_id: image.id }).subscribe(data => {
      //console.log(JSON.parse(data._body));
      this.initializeItems();
      this.api.getToastMessage('Show to customer status updated successfully');
      //loading.dismiss();
    }, error => {
      //loading.dismiss();
      this.api.showServerError();
      //console.log(error);
    })
  }

  deleteImg(image) {

    let alert = this.alertCtrl.create({
      title: 'Delete Gallery Image',
      message: 'Do you want to delete this image?',
      buttons: [
        {
          text: 'Oops, No',
          role: 'cancel',
          handler: () => {
            //console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes, Delete',
          handler: () => {
            let loading = this.loadingCtrl.create({
              content: 'Please wait...'
            });
            loading.present();
            this.api.deleteImg({ store_code: this.loggedInUser.currentStore.var_store_code, img_id: image.id }).subscribe(data => {
              //console.log(JSON.parse(data._body));
              this.initializeItems();
              loading.dismiss();
              this.api.getToastMessage('Gallery image deleted successfully');
            }, error => {
              loading.dismiss();
              this.api.showServerError();
              //console.log(error);
            })
          }
        }
      ]
    });
    alert.present();


  }

}
