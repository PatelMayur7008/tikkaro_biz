import { Component, ViewChild } from '@angular/core';
import { IonicPage, Nav, NavController, LoadingController, AlertController, ToastController } from 'ionic-angular';

import * as moment from 'moment';
import { Storage } from '@ionic/storage';
import { API } from '../../services/api-service';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { BookingViewPage } from '../booking-view/booking-view';


@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html'
})
export class DashboardPage {
  loggedInUser: any;

  activeTab: string = 'upcoming';
  @ViewChild(Nav) nav: Nav;
  currentDate: any;
  currentDateShowDay: any;
  currentDateShowDayForMain: any;
  currentDateShowMonthForMain: any;
  currentDateShowMonth: any;
  nextDate: any;
  nextDateShowDay: any;
  nextDateShowMonth: any;
  prevDate: any;
  prevDateShowDay: any;
  prevDateShowMonth: any;
  completedData: any = [];
  completedTotalBooking: any = 0;
  completedTotalPrice: any = 0;
  upcomingData: any = [];
  upcomingTotalBooking: any = 0;
  upcomingTotalPrice: any = 0;
  cancelledData: any = [];
  cancelledTotalBooking: any = 0;
  cancelledTotalPrice: any = 0;
  optionsMulti = {
    from: '2012-05-25'
  };

  weekCount: number = 0;
  startDate: any;
  weeklyData: any = [];
  public event = {
    date: moment()
  }
  public isCalenderOpen: boolean = false;
  constructor(public navCtrl: NavController,
    private storage: Storage,
    public loadingCtrl: LoadingController,
    private api: API,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private androidPermissions: AndroidPermissions

  ) {
    this.currnetDay();
    this.getData();
    this.defaultWeek();
    this.androidPermissions.requestPermissions(
      [
        androidPermissions.PERMISSION.CAMERA,
        androidPermissions.PERMISSION.CALL_PHONE,
        androidPermissions.PERMISSION.GET_ACCOUNTS,
        androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE,
        androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
      ]);
  }
  openMenu() {

  }

  currnetDay() {
    this.currentDate = moment().format('YYYY-MM-DD');
    this.currentDateShowDay = moment().format('ddd, DD');
    this.currentDateShowDayForMain = moment().format('DD');
    this.currentDateShowMonthForMain = moment().format('MMM');
    this.currentDateShowMonth = moment().format('MMM YYYY');
    // this.nextDate = moment(this.currentDate).add('days', 1);
    // this.nextDateShowDay = moment(this.currentDate).add('days', 1).format('ddd, DD');
    // this.nextDateShowMonth = moment(this.currentDate).add('days', 1).format('MMM YYYY');
    // this.prevDate = moment(this.currentDate).add('days', -1);
    // this.prevDateShowDay = moment(this.currentDate).add('days', -1).format('ddd, DD');
    // this.prevDateShowMonth = moment(this.currentDate).add('days', -1).format('MMM YYYY');
  }

  // changeDate(selectedDate) {
  //   this.nextDate = moment(selectedDate).add('days', 1);
  //   this.nextDateShowDay = moment(selectedDate).add('days', 1).format('ddd, DD');
  //   this.nextDateShowMonth = moment(selectedDate).add('days', 1).format('MMM YYYY');
  //   this.prevDate = moment(selectedDate).add('days', -1);
  //   this.prevDateShowDay = moment(selectedDate).add('days', -1).format('ddd, DD');
  //   this.prevDateShowMonth = moment(selectedDate).add('days', -1).format('MMM YYYY');
  //   this.currentDate = moment(selectedDate);
  //   this.currentDateShowDay = moment(selectedDate).format('ddd, DD');
  //   this.currentDateShowDayForMain = moment(selectedDate).format('DD');
  //   this.currentDateShowMonthForMain = moment(selectedDate).format('MMM');
  //   this.currentDateShowMonth = moment(selectedDate).format('MMM YYYY');
  //   this.updateDate();

  // }

  updateDate() {
    // console.log('activeTab', this.activeTab);
    // console.log('this.currentDate', this.currentDate);
    // console.log('this.completedData', this.completedData);
    if (this.activeTab == 'completed') {
      this.completedData.forEach(element => {
        element.timeslot.forEach(t => {
          t.timeslot = moment(t.timeslot, "HH:mm:ss").format("hh:mm A")
        });
        if (moment(element.dt_booking_date).format('DD MMMM YYYY') == moment(this.currentDate).format('DD MMMM YYYY')) {
          element.today = true;
        } else {
          element.today = false
        }
      });
    } else if (this.activeTab == 'upcoming') {
      // console.log('upcomingdata',this.upcomingData);
      this.upcomingData.forEach(element => {
        element.timeslot.forEach(t => {
          t.timeslot = moment(t.timeslot, "HH:mm:ss").format("hh:mm A")
        });
        if (moment(element.dt_booking_date).format('DD MMMM YYYY') == moment(this.currentDate).format('DD MMMM YYYY')) {
          element.today = true;
        } else {
          element.today = false
        }
      });
    } else {
      this.cancelledData.forEach(element => {
        element.timeslot.forEach(t => {
          t.timeslot = moment(t.timeslot, "HH:mm:ss").format("hh:mm A")
        });
        if (moment(element.dt_booking_date).format('DD MMMM YYYY') == moment(this.currentDate).format('DD MMMM YYYY')) {
          element.today = true
        } else {
          element.today = false
        }
      });
    }
    if (this.completedData.length > 0) {
      this.completedTotalPrice = this.completedData.map(a => {
        if (a.today == true) {
          return parseInt(a.var_price)
        } else {
          return 0;
        }
      }).reduce((a, b) => {
        return a + b;
      });
    }
    if (this.upcomingData.length > 0) {
      this.upcomingTotalPrice = this.upcomingData.map(a => {
        if (a.today == true) {
          return parseInt(a.var_price)
        } else {
          return 0;
        }
      }).reduce((a, b) => {
        return a + b;
      });
    }
    if (this.cancelledData.length > 0) {
      this.cancelledTotalPrice = this.cancelledData.map(a => {
        if (a.today == true) {
          return parseInt(a.var_price)
        } else {
          return 0;
        }
      }).reduce((a, b) => {
        return a + b;
      });
    }
    let upcomingTotalBooking = [];
    this.upcomingData.forEach(element => {
      if (element.today == true) {
        upcomingTotalBooking.push(element);
      }
    });
    this.upcomingTotalBooking = upcomingTotalBooking.length;
    let cancelledTotalBooking = [];
    this.cancelledData.forEach(element => {
      if (element.today == true) {
        cancelledTotalBooking.push(element);
      }
    });
    this.cancelledTotalBooking = cancelledTotalBooking.length;
    let completedTotalBooking = [];
    this.completedData.forEach(element => {
      if (element.today == true) {
        completedTotalBooking.push(element);
      }
    });
    this.completedTotalBooking = completedTotalBooking.length;
  }


  doneservice(dData) {
    console.log('dData', dData);
    this.api.completeBooking({ bookId: dData.id, store_id: dData.fk_store, var_price: dData.var_price }).subscribe(data => {
      console.log('dData', dData);
      this.navCtrl.setRoot(DashboardPage)
    }, error => {
      this.api.showServerError();
      console.log(error);
    })
  }
  cancleservice(dData) {
    console.log('dData', dData);
    this.api.cancelBooking1({ bookId: dData.id }).subscribe(data => {
      this.navCtrl.setRoot(DashboardPage)
    }, error => {
      this.api.showServerError();
      console.log(error);
    })
  }

  getData() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.storage.get('loggedInEmployee').then((res) => {
      this.loggedInUser = res;
      this.api.getDashBoardDate({ store_code: this.loggedInUser.currentStore.var_store_code }).subscribe(data => {
        let dData = JSON.parse(data._body).booking;
        loading.dismiss();
        console.log('dData', dData);
        dData.forEach(element => {
          if (element.enum_book_type == 'COMPLETED') {
            this.completedData.push(element);
            console.log('dData', moment(element.dt_booking_date));
          } else if (element.enum_book_type == 'UPCOMING') {
            this.upcomingData.push(element);
          } else if (element.enum_book_type == 'CANCEL') {
            this.cancelledData.push(element);
          }
        });
        this.updateDate();
      }, error => {
        loading.dismiss();
        this.api.showServerError();
        console.log(error);
      })
    })
  }

  getSum(total, num) {
    return total + num;
  }

  cancelBooking(bookingId) {
    let alert = this.alertCtrl.create({
      title: 'Confirm Cancel',
      message: 'Do you want to cancel this booking?',
      buttons: [
        {
          text: 'Close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Cancel',
          handler: () => {
            console.log('bookingId', bookingId);
            let loading = this.loadingCtrl.create({
              content: 'Please wait...'
            });
            loading.present();
            this.api.cancelBooking({ bookId: bookingId }).subscribe(data => {
              loading.dismiss();
              this.navCtrl.setRoot(this.navCtrl.getActive().component);
              let toast = this.toastCtrl.create({
                message: 'Booking Cancelled Successfully',
                duration: 2000,
                position: 'bottom'
              });
              toast.present();
              toast.onDidDismiss(() => {
                // this.navCtrl.setRoot(DashboardPage);
              });
            }, error => {
              loading.dismiss();
              this.api.showServerError();
              console.log(error);
            })
          }
        }
      ]
    });
    alert.present();
  }

  nextDateClick() {
    // this.weekCount = this.weekCount + 1;
    this.startDate = moment(this.startDate).add(1, 'weeks').startOf('isoWeek').format('YYYY-MM-DD');
    this.getWeeklyDays(this.startDate);
  }

  prevDateClick() {
    // this.weekCount = this.weekCount - 1;
    this.startDate = moment(this.startDate).add(-1, 'weeks').startOf('isoWeek').format('YYYY-MM-DD');
    this.getWeeklyDays(this.startDate);
  }

  getWeeklyDays(startDate) {
    this.weeklyData = [];
    for (let i = 0; i < 7; i++) {
      let date = moment(startDate).add(i, 'day').format('YYYY-MM-DD');
      this.weeklyData.push({
        date: date,
        showDate: moment(date).format('DD')
      })
    }
  }

  defaultWeek() {
    this.startDate = moment().startOf('isoWeek').format('YYYY-MM-DD');
    for (let i = 0; i < 7; i++) {
      let date = moment(this.startDate).add(i, 'day').format('YYYY-MM-DD');
      this.weeklyData.push({
        date: date,
        showDate: moment(date).format('DD')
      })
    }
  }

  selectNewDate(date) {
    this.currentDate = date;
    this.currentDateShowDay = moment(this.currentDate).format('ddd, DD');
    this.currentDateShowDayForMain = moment(this.currentDate).format('DD');
    this.currentDateShowMonthForMain = moment(this.currentDate).format('MMM');
    this.currentDateShowMonth = moment(this.currentDate).format('MMM YYYY');
    this.updateDate();
  }

  openCal() {
    this.isCalenderOpen = !this.isCalenderOpen;
  }

  gotoDetailsPage(booking) {
    console.log(booking);
    // return false;
    this.navCtrl.push(BookingViewPage, { booking: booking, navigate: 'dashboard' });
  }

  selectDate(e) {
    this.selectNewDate(moment(e).format('YYYY-MM-DD'));
    this.isCalenderOpen = !this.isCalenderOpen;
    this.startDate = moment(e).startOf('isoWeek').format('YYYY-MM-DD');
    this.weeklyData = [];
    for (let i = 0; i < 7; i++) {
      let date = moment(this.startDate).add(i, 'day').format('YYYY-MM-DD');
      this.weeklyData.push({
        date: date,
        showDate: moment(date).format('DD')
      })
    }
  }

  doRefresh(refresher) {
    this.navCtrl.setRoot(DashboardPage)
    refresher.complete();
  }

}
