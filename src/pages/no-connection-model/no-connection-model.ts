import { Component, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, ViewController, ToastController, ModalController, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network';
import { HomePage } from '../home/home';

@Component({
  selector: 'no-connection-model',
  templateUrl: 'no-connection-model.html'
})
export class NoConnectionPage {
  constructor(public navCtrl: NavController,
    public network: Network,
    public platform: Platform,
  ) {

  }


  doDone() {
    this.platform.exitApp();
  }


}
