import { Component, ViewChild } from '@angular/core';
import { IonicPage, Nav, NavController, LoadingController, AlertController, ToastController, NavParams } from 'ionic-angular';

import * as moment from 'moment';
import { API } from '../../services/api-service';
import { CallNumber } from '@ionic-native/call-number';
import { SocialSharing } from '@ionic-native/social-sharing';
import { EmailComposer } from '@ionic-native/email-composer';
import { DashboardPage } from '../dashboard/dashboard';

@Component({
  selector: 'page-booking-view',
  templateUrl: 'booking-view.html'
})
export class BookingViewPage {
  booking: any;
  naviagte: any;
  bookId;
  constructor(public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private navParams: NavParams,
    private socialSharing: SocialSharing,
    private toastCtrl: ToastController,
    private emailComposer: EmailComposer,
    private callNumber: CallNumber,
    private api: API,
  ) {
    this.naviagte = this.navParams.get('navigate');
    // if (this.navParams.get('navigate') == 'dashboard') {
    //   this.booking = this.navParams.get('booking');
    //   console.log('this.booking', this.booking);
    //   this.booking.dt_booking_date_format = moment(this.booking.dt_booking_date).format('DD MMMM YYYY');
    //   setTimeout(() => {
    //     this.booking.booking.var_image_thumb = this.booking.booking.var_image
    //   }, 300);
    // } else {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    if (this.navParams.get('navigate') == 'dashboard') {
      this.bookId = this.navParams.get('booking').id
    } else {
      this.bookId = this.navParams.get('booking').bookId
    }
    this.api.getBookingDetail({ booking_id: this.bookId }).subscribe(data => {
      this.booking = JSON.parse(data._body).booking;
      this.booking.dt_booking_date_format = moment(this.booking.dt_booking_date).format('DD MMMM YYYY');

      this.booking.services.forEach(element => {
        element.serviceTimeFormat = `${(element.service_hours == "00" ? '' : element.service_hours+' hr:')}${(element.service_minutes == "00" ? '': element.service_minutes+' min')}`
      });

      // setTimeout(() => {
      //   this.booking.booking.var_profile_image_thumb = this.booking.booking.var_profile_image
      // }, 300);
      loading.dismiss();
      console.log('this.booking', this.booking);
    }, error => {
      console.log(error);
      this.api.showServerError();
      loading.dismiss();

    })
    // }
    console.log('this.booking', this.booking);


  }

  call(c) {

    let alert = this.alertCtrl.create({
      title: 'Call to customer',
      message: 'Do you want to call to customer?',
      buttons: [
        {
          text: 'Oops, No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes, Continue',
          handler: () => {
            let loading = this.loadingCtrl.create({
              content: 'Please wait...'
            });
            loading.present();
            this.callNumber.callNumber(c.bint_phone, true)
            .then(() => console.log('Launched dialer!'))
            .catch(() => console.log('Error launching dialer'));
          }
        }
      ]
    });
    alert.present();
  }

  share(c) {
    console.log(c)
    this.socialSharing.shareViaWhatsApp(`${c.username}`, c.var_image).then(() => {
    }).catch(() => {
      alert("Please install Whatsapp");
    });
  }

  sendEmail(c) {
    console.log(c)
    this.emailComposer.isAvailable().then((available: boolean) => {
      console.log(available)
      let email = {
        to: c.var_email,
        // cc: '',
        // bcc: ['', ''],
        // attachments: [
        //   'file://img/logo.png',
        //   'res://icon.png',
        //   'base64:icon.png//iVBORw0KGgoAAAANSUhEUg...',
        //   'file://README.pdf'
        // ],
        // subject: 'Cordova Icons',
        // body: 'How are you? Nice greetings from Leipzig',
        isHtml: true
      };
      this.emailComposer.open(email);
    });
  }

  doneservice() {

    let alert = this.alertCtrl.create({
      title: 'Complete Booking',
      message: 'Do you want to complete this booking?',
      buttons: [
        {
          text: 'Opps, No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes, Complete',
          handler: () => {
            let loading = this.loadingCtrl.create({
              content: 'Please wait...'
            });
            loading.present();
            this.api.completeBooking({ bookId: this.bookId, store_id:this.booking.fk_store, var_price:this.booking.var_price }).subscribe(data => {
              loading.dismiss();
              // let toast = this.toastCtrl.create({
              //   message: JSON.parse(data._body).message,
              //   duration: 2000,
              //   position: 'bottom'
              // });
              // toast.present();
              // toast.onDidDismiss(() => {
              //   // this.navCtrl.setRoot(DashboardPage);
              // });
              this.api.getToastMessage('Booking Completed Successfully');
              this.navCtrl.setRoot(DashboardPage);
            }, error => {
              loading.dismiss();
              this.api.showServerError();
              console.log(error);
            })
          }
        }
      ]
    });
    alert.present();
  }
  cancleservice() {
    let alert = this.alertCtrl.create({
      title: 'Cancel Booking',
      message: 'Do you want to cancel this booking?',
      buttons: [
        {
          text: 'Opps, No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes, Cancel',
          handler: () => {
            let loading = this.loadingCtrl.create({
              content: 'Please wait...'
            });
            loading.present();
            this.api.cancelBooking1({ bookId: this.bookId }).subscribe(data => {
              loading.dismiss();
              // let toast = this.toastCtrl.create({
              //   message: JSON.parse(data._body).message,
              //   duration: 2000,
              //   position: 'bottom'
              // });
              // toast.present();
              //toast.onDidDismiss(() => {
                // this.navCtrl.setRoot(DashboardPage);
              //});
              this.api.getToastMessage('Booking Cancelled Successfully');
              this.navCtrl.setRoot(DashboardPage)
            }, error => {
              loading.dismiss();
              this.api.showServerError();
              console.log(error);
            })
          }
        }
      ]
    });
    alert.present();
  }
}
