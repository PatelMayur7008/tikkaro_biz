import { Component } from '@angular/core';
import { NavController, ViewController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { API } from '../../services/api-service';
import { utilService } from '../../services/core/util-service';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker';
import { API_BASE_URL } from '../../services/core/constants';
import { BizSubMenuPage } from '../../app/business-sub-menu/biz-sub-menu';

@Component({
  selector: 'page-business-profile',
  templateUrl: 'business-profile.html'
})
export class BusinessProfilePage {
  fileTransfer: FileTransferObject;
  public submitClicked: boolean = false;
  loggedInUser: any;
  weekoff: any;
  saloonFor: any;
  settings: any;
  allFacility: any = [];
  image: any = '';
  imageFirst: any = '';
  public event = {
    name: '',
    add: '',
    email: '',
    phone: '',
    logo: '',
    note: ''
  }

  constructor(public navCtrl: NavController,
    public viewCtrl: ViewController,
    private storage: Storage,
    private navParams: NavParams,
    private toastCtrl: ToastController,
    private utilService: utilService,
    public loadingCtrl: LoadingController,
    private api: API,
    private util: utilService,
    private transfer: FileTransfer,
    private imagePicker: ImagePicker
  ) {
    // this.storage.get('loggedInEmployee').then((res) => {
    //   this.loggedInUser = res;
    //   console.log('datasdssssss', res);
    // })
    this.getAllFacility();
    this.util.getDayNameFromNumber(3);
    this.util.getNumberFromDayName('Tuesday');
    this.fileTransfer = this.transfer.create();
  }

  getAllFacility() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.api.getAllFacility().subscribe(res => {
      loading.dismiss();
      console.log(JSON.parse(res._body))
      this.allFacility = JSON.parse(res._body);
      this.allFacility.forEach(element => {
        element.check = false;
      });
      this.getBuisneesSettings();
    }, error => {
      loading.dismiss();
      this.api.showServerError();
      console.log(error);
    })
  }

  getBuisneesSettings() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.storage.get('loggedInEmployee').then((res) => {
      this.loggedInUser = res;
      this.api.getBuisneesSettings({ store_code: this.loggedInUser.currentStore.var_store_code }).subscribe(res => {
        loading.dismiss();
        this.settings = JSON.parse(res._body).store_detail;
        console.log('ssss', this.settings.var_email);
        this.loggedInUser.currentStore.var_logo = this.settings.var_logo;
        this.loggedInUser.store_data.forEach(element => {
          if (element.var_store_code == this.loggedInUser.currentStore.var_store_code) {
            element.var_logo = this.settings.var_logo;
          }
        });
        this.storage.set('loggedInEmployee', this.loggedInUser).then((value) => {
        });
        this.event.name = (this.settings.var_title !== 'null') ? this.settings.var_title : '';
        this.event.add = (this.settings.txt_address !== 'null') ? this.settings.txt_address : '';
        this.event.email = (this.settings.var_email !== 'null') ? this.settings.var_email : '';
        this.event.phone = (this.settings.bint_phone !== 'null') ? this.settings.bint_phone : '';
        this.event.note = (this.settings.txt_note !== 'null') ? this.settings.txt_note : '';
        this.image = this.settings.var_logo;
        this.imageFirst = this.image.charAt(0);
        console.log(this.image)
        this.allFacility.forEach(element => {
          this.settings.facility.forEach(fac => {
            if (fac.id == element.id) {
              element.check = true;
            }
          });
        });
      }, error => {
        loading.dismiss();
        this.api.showServerError();
        console.log(error);
      })
    })
  }


  checked(facility) {
    console.log(facility);
    this.allFacility = facility;
  }

  updateBuisneesSettings() {
    this.submitClicked = true;
    if (this.image != '' && this.event.add != '' && this.event.name != '' && this.event.email != '' && this.event.phone != '' && this.event.note != '') {
      console.log(this.image);
      this.imageFirst = this.image.charAt(0);
      // console.log(API_BASE_URL + 'updateBussinessProfile');

      let facility: any = [];
      this.allFacility.forEach(element => {
        if (element.check == true) {
          facility.push({ id: element.id })
        }
      });
      if (this.imageFirst == 'h') {
        console.log('https');
        let payload = {
          store_code: this.loggedInUser.currentStore.var_store_code,
          var_title: this.event.name,
          txt_address: this.event.add,
          var_email: this.event.email,
          bint_phone: this.event.phone,
          txt_note: this.event.note,
          facility: facility,
          var_latitude: "",
          var_longitude: "",
          int_areacode: "",
        }
        console.log(payload);
        // this.event.store_id = this.loggedInUser.currentStore.var_store_code;
        let loading = this.loadingCtrl.create({
          content: 'Please wait...'
        });
        loading.present();
        this.api.updateBuisneesSettings(payload).subscribe(res => {
          loading.dismiss();
          this.getBuisneesSettings();
          this.navCtrl.setRoot(BizSubMenuPage);
          this.api.getToastMessage('General settings updated successfully');
        }, error => {
          console.log(error);
        })
      } else {
        console.log('files');
        let loader = this.loadingCtrl.create({
          content: "Uploading..."
        });
        loader.present();
        let options: FileUploadOptions = {
          fileKey: 'file',
          fileName: `receipt_pic.jpg`,
          httpMethod: 'POST',
          mimeType: 'image/jpg',
          chunkedMode: false,
          params: {
            store_code: this.loggedInUser.currentStore.var_store_code,
            var_title: this.event.name,
            txt_address: this.event.add,
            var_email: this.event.email,
            bint_phone: this.event.phone,
            txt_note: this.event.note,
            facility: facility,
            var_latitude: "",
            var_longitude: "",
            int_areacode: ""
          },
          headers: {
            "Accept": "image/jpg",
            'Authorization': 'bearer ' + this.utilService.getToken(),
          }
        };
        console.log('fdsfsdfsdfs');
        console.log(this.image);
        console.log(typeof (this.image))

        this.fileTransfer.upload(this.image, API_BASE_URL + '/updateBussinessProfileWithImage', options)
          .then((data) => {
            // alert("success" + JSON.stringify(data));
            console.log(JSON.stringify(data));
            console.log(JSON.parse(JSON.stringify(data)));
            let res = JSON.parse(JSON.parse(JSON.stringify(data)).response);
            loader.dismiss();
            this.getBuisneesSettings();
            this.presentToast(res.message);
          }, (err) => {
            console.log('error', err);
            loader.dismiss();
            this.presentToast(err);
          });
      }
    }

  }

  selectImage() {
    let options: ImagePickerOptions = {
      maximumImagesCount: 1
    };
    this.imagePicker.getPictures(options).then((results) => {
      for (var i = 0; i < results.length; i++) {
        this.image = results[i];
        this.imageFirst = this.image.charAt(0);
      }
    }, (err) => { });
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
      this.getBuisneesSettings();
    });

    toast.present();
  }


}
