import { Component } from '@angular/core';
import { NavController, ViewController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import * as moment from 'moment';
import { Storage } from '@ionic/storage';
import { API } from '../../services/api-service';
import { utilService } from '../../services/core/util-service';

@Component({
  selector: 'page-timings',
  templateUrl: 'timings.html'
})
export class TimingsPage {
  public event = {
    date: moment().format('YYYY-MM-DD'),
    timeStarts: moment().format('HH:mm'),
    timeEnds: moment().format('HH:mm'),
    text: ''
  }
  loggedInUser: any;

  timigs: any = [];
  constructor(public navCtrl: NavController,
    public viewCtrl: ViewController,
    private storage: Storage,
    private navParams: NavParams,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private api: API,
    private util: utilService,
  ) {
    this.getTimigs();
  }


  getTimigs() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.storage.get('loggedInEmployee').then((res) => {
      this.loggedInUser = res;
      console.log('this.loggedInUser', this.loggedInUser);
      this.api.getTimigs({ store_code: this.loggedInUser.currentStore.var_store_code }).subscribe(res => {
        loading.dismiss();
        console.log(JSON.parse(res._body).data,'Timings');
        this.timigs = JSON.parse(res._body).data;
      }, error => {
        loading.dismiss();
        this.api.showServerError();
        console.log(error);
      })
    })

  }

  updateTimings(timigs) {
    console.log(timigs);
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.api.updateTimings(timigs).subscribe(res => {
      var res1 = JSON.parse(res._body);
      console.log(res1.status,'res');
      loading.dismiss();
      let toast = this.toastCtrl.create({
        message: res1.message,
        duration: 2000,
        position: 'bottom'
      });
      toast.present();
      toast.onDidDismiss(() => {
        
      }); 
    }, error => {
      loading.dismiss();
      this.api.showServerError();
      console.log(error);
    })

  }
}
