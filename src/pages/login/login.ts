declare var Pushy: any;
declare var SMS:any;
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Platform } from 'ionic-angular';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HomePage } from '../home/home';
import { SystemService } from '../../services/system-service';
import { DashboardPage } from '../dashboard/dashboard';
import { Storage } from '@ionic/storage';
import { API } from '../../services/api-service';
import { AndroidPermissions } from '@ionic-native/android-permissions';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  public loginForm: FormGroup;
  public otpForm: FormGroup;
  public isOTPSend: boolean = false;
  public submitClicked: boolean = false;
  public submitOTPClicked: boolean = false;
  public otpCheck: any = 0;
  public otpFail: any;
  public userdata: any;
  public mobileNumbershow: any;
  public maskMobileNumber: any;
  public resendOtp: any;
  public seconds : number = 60;
  public isEnableResend : boolean;

  constructor(public navCtrl: NavController,
    private formBuilder: FormBuilder,
    public navParams: NavParams,
    public androidPermissions: AndroidPermissions,
    public api: API,
    public platform:Platform,
    private storage: Storage,
    public loadingCtrl: LoadingController,
    private systemService: SystemService
  ) {
    this.createForm();
  }

  ionViewWillEnter() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_SMS).then(
      success => console.log('Permission granted'),
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_SMS)
    );
    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_SMS]);
  }

  readListSMS() {
    this.platform.ready().then((readySource) => {
      let filter = {
        box: 'inbox', // 'inbox' (default), 'sent', 'draft'
        indexFrom: 0, // start from index 0
        maxCount: 100, // count of SMS to return each time
      };
      if (SMS) SMS.listSMS(filter, (ListSms) => {
        console.log("Sms", ListSms);
      },
        Error => {
          console.log('error list sms: ' + Error);
        });
    });

    if (SMS) SMS.startWatch(() => {
      console.log('watching started');
    }, Error => {
      console.log('failed to start watching');
    });

    document.addEventListener('onSMSArrive', (e: any) => {
      var sms = e.data;
      console.log(sms);
      this.otpForm.controls['otp'].setValue('hhh');
    });

  }


  loginFormSubmit() {
    this.submitClicked = true;
    this.mobileNumbershow = this.loginForm.controls.phone_number.value;
    this.maskMobileNumber = (this.mobileNumbershow).slice(-4);
    var isValid = true;
    if(this.api.getRequiredValidation(this.mobileNumbershow) || 
      this.api.getMobileNumberValidation(this.mobileNumbershow)) {
        isValid = false;
    }
    console.log(this.loginForm)
    if (this.loginForm.status == 'VALID' && isValid) {
      console.log(this.loginForm.controls.phone_number.value);
      let payload = this.loginForm.value;
      payload.otp = this.getRandomInt();
      console.log(this.loginForm['phone_number']);
      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
      loading.present();
      this.api.loginFormSubmit(payload).pipe().subscribe(response => {
        this.api.getloginDetails(payload).subscribe(response => {
          this.userdata = JSON.parse(response._body);
          console.log('this.userdata',this.userdata);
          setTimeout(() => {
            loading.dismiss();
          }, 100);
          this.setOtpTimeout();
          if (this.userdata.data) {
            console.log(this.userdata.token)            
            this.storage.set('loggedinToken', this.userdata.token).then((res) => {
              this.api.onChangeLoggedInUser(this.userdata.token);
              if (this.userdata.data.int_otp) {
                this.otpCheck = this.userdata.data.int_otp;
                // this.loginForm.reset();
                this.isOTPSend = true;
                this.readListSMS();
                // this.api.sentText(this.userdata.data.var_phone, this.userdata.data.int_otp).subscribe(response => {
                //   let res = JSON.parse(response._body)
                //   loading.dismiss();
                //   if (res.status == 'success') {
                //     this.isOTPSend = true;
                //   } else {
                //     this.otpCheck = 'something went wrong';
                //   }
                // }, error => {
                //   loading.dismiss();
                //   console.log(error);
                // })
              }
            })
          } else {
            this.userdata.showMessage = this.userdata.message;
          }
        }, error => {
          setTimeout(() => {
            loading.dismiss();
          }, 100);
          this.api.showServerError();
          console.log(error);
        })
      }, error => {
        console.log(error);
      })
    }

  }

  setOtpTimeout() {
    this.isEnableResend = false;
      this.seconds = 60;
      var timer = setInterval(() => {
      if(this.seconds != 0) {
        this.seconds -=  1;
        this.isEnableResend = false;
      } else {
        clearInterval(timer);
        this.isEnableResend = true;
      }
     }, 1000);
  }

  backBuuton() {
    this.isOTPSend = !this.isOTPSend;
  }

  otpFormSubmit() {
    this.submitOTPClicked = true;
    if (this.otpForm.status == 'VALID') {
      let that = this;
      let payload = this.otpForm.value;
      if (payload.otp == this.otpCheck) {
        this.userdata.store_data.forEach(element => {
          if (element.var_title.indexOf(' ') !== -1) {
            element.shortName = element.var_title.split(" ")[0].charAt(0) + element.var_title.split(" ")[1].charAt(0);
          } else {
            element.shortName = element.var_title.split(" ")[0].charAt(0) + element.var_title.split(" ")[0].charAt(1);
          }
        });
        this.userdata.currentStore = this.userdata.store_data[0];
        this.systemService.setUserdata(this.userdata).subscribe((res) => {
          this.storage.get('loggedInEmployee').then((res) => {
            document.addEventListener('deviceready', function () {
              Pushy.register(function (err, deviceToken) {
                if (err) {
                  return alert(err);
                }
                alert(res.data.id + '--' + deviceToken)
                that.api.sentPushyId({ user_id: res.data.id, pushyId: deviceToken }).subscribe((res) => {
                  console.log(res)
                })
              });
            });
          })
          this.navCtrl.setRoot(DashboardPage);
        });
      } else {
        this.otpFail = 'Your OTP is Wrong';
      }
    }


  }

  getRandomInt() {
    let random = Math.floor(100000 + Math.random() * 900000);
    return random;
  }



  private createForm() {
    this.loginForm = this.formBuilder.group({
      store_code: ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(6)])],
      phone_number: ['', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])]
    });

    this.otpForm = this.formBuilder.group({
      otp: ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(6)])],
    });
  }

}
