import { Component } from '@angular/core';
import * as moment from 'moment';
import { IonicPage, ViewController, NavParams, Nav, NavController, ModalController, LoadingController, ToastController } from 'ionic-angular';
import { API } from '../../../services/api-service';
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker';
import { FileTransfer, FileTransferObject, FileUploadOptions } from '@ionic-native/file-transfer';
import { API_BASE_URL } from '../../../services/core/constants';
import { Storage } from '@ionic/storage';
import { utilService } from '../../../services/core/util-service';

@Component({
  selector: 'page-add-portfolio',
  templateUrl: 'add-portfolio.html'
})
export class AddPortfolio {
  fileTransfer: FileTransferObject;
  data;
  loggedInUser: any;
  userDetails;
  event: any = {
    title: ''
  }
  image: any = '';
  public submitClicked: boolean = false;
  constructor(public navCtrl: NavController,
    public viewCtrl: ViewController,
    private navParams: NavParams,
    private toastCtrl: ToastController,
    private transfer: FileTransfer,
    private utilService: utilService,
    private imagePicker: ImagePicker,
    private storage: Storage,

    public loadingCtrl: LoadingController,
    private api: API,
  ) {
    this.fileTransfer = this.transfer.create();
    this.storage.get('loggedInEmployee').then((res) => {
      this.loggedInUser = res;
    })
  }

  addPortfolio() {
    this.submitClicked = true;
    if (this.image != '') {
      if (this.event.title != '') {
        let loading = this.loadingCtrl.create({
          content: 'Please wait...'
        });
        loading.present();
        let options: FileUploadOptions = {
          fileKey: 'file',
          fileName: `portfolio.jpg`,
          httpMethod: 'POST',
          mimeType: 'image/jpg',
          chunkedMode: false,
          params: {
            store_id: this.loggedInUser.currentStore.var_store_code,
            title: this.event.title,
          },
          headers: { "Accept": "image/jpg",
          'Authorization': 'bearer ' + this.utilService.getToken(),
        }
        };
        this.fileTransfer.upload(this.image, API_BASE_URL + '/addStorePortfolioWithImage', options)
          .then((data) => {
            loading.dismiss();
            let res = JSON.parse(data.response);
            console.log(res.message)
            this.presentToast(res.message);
            this.viewCtrl.dismiss();
          }, (err) => {
            loading.dismiss();
            console.log('error', err.response);
            let res = JSON.parse(err.response);
            this.presentToast(res.message);
            this.viewCtrl.dismiss();
          });
      } else {
        this.presentToast('Please add title');
      }
    } else {
      this.presentToast('Please select image');
    }


  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  selectImage() {
    let options1: ImagePickerOptions = {
      maximumImagesCount: 1
    };
    this.imagePicker.getPictures(options1).then((results) => {
      for (var i = 0; i < results.length; i++) {
        this.image = results[i];
      }
    }, (err) => { });

  }


  closeModal() {
    this.viewCtrl.dismiss();
  }



}
