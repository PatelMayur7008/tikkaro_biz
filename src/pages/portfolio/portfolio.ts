import * as moment from 'moment';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, Nav, NavController, ModalController, LoadingController, ToastController, AlertController } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { API } from '../../services/api-service';
import { AddPortfolio } from './add-portfolio/add-portfolio';

@Component({
  selector: 'page-portfolio',
  templateUrl: 'portfolio.html'
})

export class PortfolioPage {
  loggedInUser: any;
  portfolios: any = [];
  isEnabled: boolean = false;
  constructor(public navCtrl: NavController,
    public modalCtrl: ModalController,
    private storage: Storage,
    private toastCtrl: ToastController,
    private alertCtrl: AlertController,
    public loadingCtrl: LoadingController,

    private api: API,

  ) {
    this.initializeItems();
  }

  addPortfolio() {
    let modal = this.modalCtrl.create(AddPortfolio);
    modal.present();
    modal.onDidDismiss(data => {
      console.log(data);
      this.initializeItems();

    });
  }


  initializeItems() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.storage.get('loggedInEmployee').then((res) => {
      this.loggedInUser = res;
      this.api.getStorePortfolio({ store_id: this.loggedInUser.currentStore.var_store_code }).subscribe(data => {
        this.portfolios = JSON.parse(data._body);
        this.portfolios.forEach(element => {
          setTimeout(() => {
            element.var_file_thumb = element.var_file;
          }, 500);
        });
        console.log(this.portfolios,'Test Portfolio ');
        loading.dismiss();
      }, error => {
        loading.dismiss();
        this.api.showServerError();
        console.log(error);
        loading.dismiss();

      })
    })
  }

  changeStatus(p) {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    //loading.present();
    this.api.togglePortfolioStatus({ portfolio_id: p.id }).subscribe(data => {
      //loading.dismiss();
      this.initializeItems();
      this.api.getToastMessage('Show to customer status updated successfully');
    }, error => {
      //loading.dismiss();
      this.api.showServerError();
      console.log(error);
    })
  }

  deletePortfolio(p) {
    let alert = this.alertCtrl.create({
      title: 'Delete Image',
      message: 'Do you want to delete this image?',
      buttons: [
        {
          text: 'Oops, No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes, Delete',
          handler: () => {
            let loading = this.loadingCtrl.create({
              content: 'Please wait...'
            });
            loading.present();
            this.api.deletePortfolio({ portfolio_id: p.id }).subscribe(data => {
              loading.dismiss();
              this.initializeItems();
              this.api.getToastMessage('Portfolio image deleted successfully');
            }, error => {
              loading.dismiss();
              this.api.showServerError();
              console.log(error);
            })

          }
        }
      ]
    });
    alert.present();
  }


}
