import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';

import { APP_VERSION, FORCE_REINIT } from './core/constants';

@Injectable()
export class SystemService {
  // private syncServiceCount = 0;

  constructor(private storage: Storage,

  ) { };

  initializeApplication(): Observable<any> {
    return new Observable(ob => {
      this.getAppVersion().subscribe((appVersion) => {
        this.storage.get('isSystemInitialized').then((res) => {
          if (res == null) {
            this.storage.set('isSystemInitialized', true).then((value) => {
              this.storage.set('isTerminalRegistered', false).then((value) => {
                this.reInitializeApplication().subscribe(() => ob.next());
              });
            });
          } else {
            if (appVersion != APP_VERSION) {
              this.setAppVersion().subscribe((newAppVersion) => {
                this.reInitializeApplication().subscribe(() => ob.next());
              });
            } else {
              ob.next();
            }
          }
        })
      })
    })
  }

  reInitializeApplication(): Observable<any> {
    return new Observable(ob => {
      if (FORCE_REINIT == true) {
        this.storage.set('isTerminalRegistered', false).then((value) => {
          this.employeeLogout().subscribe(() => {
            ob.next();
          });
        });
        //do code for reinitialize the application
      } else {
        ob.next();
      }
    });
  }

  getAppVersion(): Observable<any> {
    return new Observable(ob => {
      this.storage.get('appVersion').then((value) => {
        if (value != null) {
          ob.next(value);
        } else {
          this.setAppVersion().subscribe((value) => {
            ob.next(value);
          });
        }
      });
    })
  }

  setAppVersion(): Observable<any> {
    return new Observable(ob => {
      this.storage.set('appVersion', APP_VERSION).then((value) => {
        ob.next(value);
      });
    })
  }

  employeeLogin(data): Observable<any> {
    return new Observable(ob => {
      this.storage.set('isEmployeeLoggedIn', true).then((value) => {
        this.storage.set('loggedInEmployee', data).then((value) => {
          this.storage.set('isTerminalRegistered', false).then((value) => {
            ob.next(value);
          });
        });
      });
    });
  }




  employeeLogout(): Observable<any> {
    return new Observable(ob => {
      this.storage.set('isEmployeeLoggedIn', false).then((value) => {
        this.storage.set('loggedInEmployee', '').then((value) => {
          this.storage.set('loggedinToken', '').then((value) => {
            ob.next(value);
          });
        });
      });
    });
  }

  getLoggedInEmployee(): Observable<any> {
    return new Observable(ob => {
      this.storage.get('loggedInEmployee').then((res) => {
        ob.next(res);
      })
    })
  }

  checkLastUpdatedDate(): Observable<any> {
    return new Observable(ob => {
      this.storage.get('lastUpdateDate').then((res) => {
        let LastOpenTime = res;
        ob.next()
      })
    })
  }

  setUserdata(userdata: any): Observable<any> {
    return new Observable(ob => {
      this.storage.set('isEmployeeLoggedIn', true).then((res) => {
        this.storage.set('loggedInEmployee', userdata).then((res) => {
          this.storage.set('lastUpdateDate', '').then((res) => {
            ob.next();
          })
        });
      })
    })
  }


}
