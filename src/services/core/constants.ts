// export const API_BASE_URL: string = `http://9b9a61d5.ngrok.io/tikkaro-api/v1/apibiz`;

// export const API_BASE_URL: string = `http://shiningvehicles.com/api/apibiz`;

// export const API_BASE_URL: string = `https://tikseat.com/tik-admin/apibiz`;
export const API_BASE_URL: string = `https://tikseat.com/securetikapi/v1/apibiz`;

// export const STORE_IMAGE_URL: string = `/public/upload/store_pic/`;     remove concat of image
// export const LOGO_URL: string = `/public/upload/store_logo/`;

// export const API_BASE_URL: string = `http://localhost:8080/tikkaro/apibiz`;//7021a193.ngrok.io http://2dcca45b.ngrok.io
export const APP_VERSION: string = '0.0.1';
export const FORCE_REINIT: boolean = true;
// export const ENV = {
//   production: false,
//   serverUrl: API_BASE_URLhttp://fa215158.ngrok.io http://d42e778c.ngrok.io
// };
export const API_END_POINTS: any = {
  login: '/authentication',
  checkLogin: '/checkAuthentication',
  getStore: '/getStoreList',
  getStoreDetails: '/getStoreListDetailById',
  getTimeSlotsByStoreId: '/getTimeSlotsByStoreId',
  addBank: '/addBankDetails',
  getDashBoardDate: '/getBookingRecordByStoreCode',
  getHolidays: '/getStoreHoliday',
  deleteHolidays: '/deleteStoreHoliday',
  addHolidays: '/addStoreHoliday',
  getStaffList: '/getAllStaffByStoreId',
  addStaff: '/addStoreStaffWithoutImage',
  updateStaff: '/updateStoreStaffWithoutImage',
  deleteStaff: '/deleteStoreStaff',
  getSettings: '/getStoreSetting',
  updateSettings: '/updateStoreSetting',
  getBuisneesSettings: '/getStoreDetailById',
  getAllFacility: '/getAllFacility',
  updateBuisneesSettings: '/updateBussinessProfileWithoutImage',
  getTimigs: '/getStoreTiming',
  updateTimings: '/updateStoreTiming',
  getServciesList: '/getStoreService',
  deleteServices: '/deleteStoreService',
  getAllServciesList: '/getAllservice',
  updateServices: '/updateStoreService',
  getCustomerList: '/getCustomerStats',
  getCustomerDetailsList: '/getCustomerBookingDetail',
  getImages: '/getStoreImages',
  getStorePackage: '/getStorePackage',
  addStorePackage: '/addStorePackage',
  getStoreService: '/getStoreService',
  addPackage: '/addStorePackage',
  deletePackage: '/deletePackage',
  setProfile: '/makeImageAsProfile',
  activeImg: '/makeImageActiveInActive',
  deleteImg: '/deleteStoreImage',
  getStoreOffers: '/getStoreOffer',
  addStoreOffers: '/addStoreOffer',
  deleteOffer: '/deleteOffer',
  getBankDetails: '/getBankDetails',
  sentPushyId: '/sentPushyId',
  getStorePortfolio: '/getStorePortfolio',
  togglePortfolioStatus: '/togglePortfolioStatus',
  deletePortfolio: '/deletePortfolio',
  completeBooking: '/completeBooking',
  cancelBooking: '/cancelBooking',
  getBookingDetail: '/getBookingDetail',
  getPaymentHistory: '/getPaymentHistory',
  requestPayment: '/requestPayment',
  getStoreTiming: '/getStoreTiming',
  getHolidayList: '/getHolidayList',
};


export const BIZ_VALIDATION_MESSAGE: any = {
  required: 'This is required field',
  mobile: 'Please enter valid mobile number',
  number: 'Please enter only numbers',
}

