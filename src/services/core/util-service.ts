import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class utilService {
  loggedinToken;
  constructor(private storage: Storage,
    private http: Http,
  ) {
    this.storage.get('loggedinToken').then((loggedinToken) => {
      console.log(loggedinToken);
      this.loggedinToken = loggedinToken;
    });
  };

  getDayNameFromNumber(day) {
    day = parseInt(day);
    switch (day) {
      case 0:
        day = "Sunday";
        break;
      case 1:
        day = "Monday";
        break;
      case 2:
        day = "Tuesday";
        break;
      case 3:
        day = "Wednesday";
        break;
      case 4:
        day = "Thursday";
        break;
      case 5:
        day = "Friday";
        break;
      case 6:
        day = "Saturday";
    }
    return day;

  }

  getNumberFromDayName(day) {
    switch (day) {
      case 'Sunday':
        day = 0;
        break;
      case 'Monday':
        day = 1;
        break;
      case 'Tuesday':
        day = 2;
        break;
      case 'Wednesday':
        day = 3;
        break;
      case 'Thursday':
        day = 4;
        break;
      case 'Friday':
        day = 5;
        break;
      case 'Saturday':
        day = 6;
    }
    return day;
  }

  getToken(){
    return this.loggedinToken;
  }

}
