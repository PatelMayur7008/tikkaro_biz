import { Http, Headers } from '@angular/http';
import { Injectable, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';

import { APP_VERSION, FORCE_REINIT, API_BASE_URL, API_END_POINTS, BIZ_VALIDATION_MESSAGE } from './core/constants';
import { utilService } from './core/util-service';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
// import { Headers } from '@angular/http/src/headers';

@Injectable()
export class API {
  private apiEndPoint: any = API_END_POINTS;
  private validation_msg: any = BIZ_VALIDATION_MESSAGE;
  private API_BASE_URL: any = API_BASE_URL;
  headers;
  constructor(private storage: Storage,
    private http: Http,
    private util: utilService,
    private toastCtrl: ToastController,
  ) {
    this.storage.get('loggedinToken').then((loggedinToken) => {
      console.log(loggedinToken)
      this.headers = '';
      this.headers = new Headers({
        'Content-Type': 'application/json',
        'Authorization': 'bearer ' + loggedinToken,
      });
      // this.headers.append('Authorization', 'bearer ' + loggedinToken);
    });
  };

  onChangeLoggedInUser(loggedinToken) {
    this.headers = '';
    this.headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'bearer ' + loggedinToken,
    });
    // this.headers.append('Authorization', 'bearer ' + loggedinToken);
  }

  loginFormSubmit(payload: any): Observable<any> {
    // return this.http.post(`${API_BASE_URL}${this.apiEndPoint.login}`, payload);
    // return this.http.post(`http://9b9a61d5.ngrok.io/tikkaro-api/v1/authcontroller/authentication`, payload);
    return this.http.post(`https://tikseat.com/securetikapi/v1/authcontroller/authentication`, payload);
  }

  getloginDetails(payload: any): Observable<any> {
    // return this.http.post(`http://9b9a61d5.ngrok.io/tikkaro-api/v1/authcontroller/checkAuthentication`, payload);
    return this.http.post(`https://tikseat.com/securetikapi/v1/authcontroller/checkAuthentication`, payload);
    // return this.http.post(`${API_BASE_URL}${this.apiEndPoint.checkLogin}`, payload);
  }

  addBank(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.addBank}`, payload, { headers: this.headers });
  }

  getDashBoardDate(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.getDashBoardDate}`, payload, { headers: this.headers });
  }

  completeBooking(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.completeBooking}`, payload, { headers: this.headers });
  }

  cancelBooking1(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.cancelBooking}`, payload, { headers: this.headers });
  }

  getHolidays(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.getHolidays}`, payload, { headers: this.headers });
  }

  deleteHolidays(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.deleteHolidays}`, payload, { headers: this.headers });
  }

  getStoreTiming(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.getStoreTiming}`, payload, { headers: this.headers });
  }

  getCustomerDetailsList(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.getCustomerDetailsList}`, payload, { headers: this.headers });
  }

  getSettings(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.getSettings}`, payload, { headers: this.headers });
  }

  updateSettings(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.updateSettings}`, payload, { headers: this.headers });
  }

  getBuisneesSettings(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.getBuisneesSettings}`, payload, { headers: this.headers });
  }

  getAllFacility(): Observable<any> {
    return this.http.get(`${API_BASE_URL}${this.apiEndPoint.getAllFacility}`, { headers: this.headers });
  }

  addHolidays(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.addHolidays}`, payload, { headers: this.headers });
  }

  getStorePackage(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.getStorePackage}`, payload, { headers: this.headers });
  }

  getStoreOffers(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.getStoreOffers}`, payload, { headers: this.headers });
  }

  addStoreOffers(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.addStoreOffers}`, payload, { headers: this.headers });
  }

  deleteOffer(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.deleteOffer}`, payload, { headers: this.headers });
  }

  addStorePackage(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.addStorePackage}`, payload, { headers: this.headers });
  }

  getTimigs(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.getTimigs}`, payload, { headers: this.headers });
  }

  getStoreService(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.getStoreService}`, payload, { headers: this.headers });
  }

  deletePackage(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.deletePackage}`, payload, { headers: this.headers });
  }

  addPackage(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.addPackage}`, payload, { headers: this.headers });
  }

  addStaff(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.addStaff}`, payload, { headers: this.headers });
  }

  getStaffList(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.getStaffList}`, payload, { headers: this.headers });
  }

  getServciesList(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.getServciesList}`, payload, { headers: this.headers });
  }

  getAllServciesList(): Observable<any> {
    return this.http.get(`${API_BASE_URL}${this.apiEndPoint.getAllServciesList}`, { headers: this.headers });
  }

  deleteServices(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.deleteServices}`, payload, { headers: this.headers });
  }

  getCustomerList(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.getCustomerList}`, payload, { headers: this.headers });
  }

  getHolidayList(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.getHolidayList}`, payload, { headers: this.headers });
  }

  getImages(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.getImages}`, payload, { headers: this.headers });
  }

  setProfile(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.setProfile}`, payload, { headers: this.headers });
  }

  activeImg(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.activeImg}`, payload, { headers: this.headers });
  }

  deleteImg(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.deleteImg}`, payload, { headers: this.headers });
  }

  updateServices(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.updateServices}`, payload, { headers: this.headers });
  }

  updateStaff(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.updateStaff}`, payload, { headers: this.headers });
  }

  updateTimings(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.updateTimings}`, payload, { headers: this.headers });
  }

  updateBuisneesSettings(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.updateBuisneesSettings}`, payload, { headers: this.headers });
  }

  deleteStaff(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.deleteStaff}`, payload, { headers: this.headers });
  }

  getStore(): Observable<any> {
    return this.http.get(`${API_BASE_URL}${this.apiEndPoint.getStore}`, { headers: this.headers });
  }

  getStoreDetails(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.getStoreDetails}`, payload, { headers: this.headers });
  }

  getTimeSlotsByStoreId(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.getTimeSlotsByStoreId}`, payload, { headers: this.headers });
  }

  cancelBooking(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.cancelBooking}`, payload, { headers: this.headers });
  }

  getBankDetails(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.getBankDetails}`, payload, { headers: this.headers });
  }

  sentPushyId(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.sentPushyId}`, payload, { headers: this.headers });
  }
  getStorePortfolio(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.getStorePortfolio}`, payload, { headers: this.headers });
  }
  togglePortfolioStatus(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.togglePortfolioStatus}`, payload, { headers: this.headers });
  }
  deletePortfolio(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.deletePortfolio}`, payload, { headers: this.headers });
  }

  getBookingDetail(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.getBookingDetail}`, payload, { headers: this.headers });
  }

  getPaymentHistory(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.getPaymentHistory}`, payload, { headers: this.headers });
  }

  requestPayment(payload: any): Observable<any> {
    return this.http.post(`${API_BASE_URL}${this.apiEndPoint.requestPayment}`, payload, { headers: this.headers });
  }

  sentText(number, message): Observable<any> {
    return this.http.get(`http://api.textlocal.in/send/?username=tikseat@gmail.com&hash=35fbe6483f29d378bb2f61556a4f9c26c092c612af2649e5068f28ff839e440e&sender=TXTLCL&numbers=${number}&message=${message}`);
  }

  showServerError() {
    let toast = this.toastCtrl.create({
      message: 'Server Error',
      position: 'bottom',
      duration: 3000
    });
    toast.present();
  }

  getValidationMessage(data, type) {
    // var self = this;
    // var validation = '';
    // var keepGoing = true;
    // var validations = type.split(',');
    // validations.forEach(function(element) {
    //   if(keepGoing) {
    //     if(element == 'required') {
    //       validation = self.requiredValidation(data);
    //       keepGoing = false;
    //     } else if(element == 'number') {
    //       validation = self.numberValidation(data)
    //       keepGoing = false;
    //     }
    //   }
    // });

    //return validation;
  }

  getRequiredValidation(data) {
    var validation = '';
    if(data == '') {
      validation = this.validation_msg['required'];
    }
    return validation;
  }

  getNumberValidation(data) {
    var validation = '';
    if(isNaN(data)) {
      validation = this.validation_msg['number'];
    }
    return validation;
  }

  getMobileNumberValidation(data) {
    var validation = '';
    if(isNaN(data) ||  (data.length < 10)) {
      validation = this.validation_msg['mobile'];
    }
    return validation;
  }

  getToastMessage(message) {
    let toast = this.toastCtrl.create({
      message: message,
      position: 'bottom',
      duration: 3000
    });
    return toast.present();
  }
}
